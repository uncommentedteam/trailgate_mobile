export const environment = {
  production: true,
  root_url: "https://gateway.portmaputo.com/api",
  gatekeeper_cod: 55,
  core_cod:53,
  thread_cod: 52,
  host_cod: 51,
  mre_obs:'MRE',
  mrs_obs:'MRS',
  mrp_obs:'MRP',
  rpp_cod:'RPP',
  gate_cod:'GAT',
  fun_cod:'FUN',
  pvc_cod:'PVC',
  tdi_cod:'TDI',
  tid_cod:'TID',
  default_gate_cod: 70
}
