// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //root_url: "http://172.16.220.60:82/api",
  // root_url: "http://test.portmaputo.com:82/api",
  root_url: "https://gateway.portmaputo.com/api",
  // root_url: "http://10.2.1.220:5000/api",
  // root_url: "http://localhost:5000/api",
  // root_url: "http://localhost:5001/api",
  // root_url: 'http://192.168.237.104:5000/api',
  // Agente de segurança do portão
  gatekeeper_cod: 55,
  // Gestor do departamento de segurança interna
  core_cod: 53,
  // Oficial de segurança
  thread_cod: 52,
  // Colaborador
  host_cod: 51,
  mre_obs: 'MRE',
  mrs_obs: 'MRS',
  mrp_obs: 'MRP',
  rpp_cod: 'RPP',
  gate_cod: 'GAT',
  fun_cod: 'FUN',
  pvc_cod: 'PVC',
  tdi_cod: 'TDI',
  tid_cod: 'TID',
  default_gate_cod: 70
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
