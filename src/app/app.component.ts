import { Subscription } from 'rxjs';
import { Component } from '@angular/core';

import { MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { User } from './top/user';
import { environment } from 'src/environments/environment';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  user:User
  env = environment
  authSub: Subscription
  gateSub: Subscription
  hasGate: boolean = false
  gate:any = null

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // For side menu
    private menu: MenuController,
    private router: Router,
    private authService: AuthService
    
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  onDashboardClicked(){
    this.menu.close()
    this.router.navigate(['htc','dashboard'])
  }

  onRequestClicked(){
    this.menu.close()
    this.router.navigate(['htc','pedido'])
  }
  
  onUserInfoClicked(){
    this.menu.close()
    this.router.navigate(['htc','dados'])
  }

  onLogOutClicked(){
    this.menu.close()
    this.hasGate = false
    this.gate = null
    this.authService.LogOut()
  }

  onGateClicked(){
    this.menu.close()
    this.router.navigate(['gatekeeper','gates'])

  }

  onPsClicked(){
    this.menu.close()
    this.router.navigate(['gatekeeper','ps'])
  }
  
  onEntradasClicked(){
    this.menu.close()
    this.router.navigate(['gatekeeper','io','1'])
  }

  onSaidasClicked(){
    this.menu.close()
    this.router.navigate(['gatekeeper','io', '0'])
  }

  ngOnInit() {
    this.authSub = this.authService.isAuth.subscribe(isAuth => {
      if (isAuth){
        this.user = this.authService.getUser()    
      } else {
        this.user = this.authService.getUser()    
        console.log("Leave")
      }
    })

    this.gateSub = this.authService.gateListener.subscribe(gateObj => {
      if (gateObj !== null){
        this.hasGate = true
        this.gate = gateObj
      }else {
        this.hasGate = false
      }
      
    })
    
  }

  ngOnDestroy() {
    this.authSub.unsubscribe()
    this.gateSub.unsubscribe()
  }

}
