import { UserVisitor } from './../top/user_visitor';
import { ExtraVisit } from './../top/extra_visit';
import { User } from './../top/user';
import { Terminal } from './../top/terminal';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Department_Filter } from '../top/department_filter';
import { AlertController, ToastController } from '@ionic/angular';
// import * as socketIo from 'socket.io-client'
import * as moment from 'moment';
import { Pedido } from '../top/pedido';
import { PedidoInfo } from '../top/pedido_info';
import { Extra } from '../top/extra';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class HtcService {

  URL_VISITS = environment.root_url+"/visits"

  // 0 - Dashboard [Title]
  // 1 - Pedidos [Search]
  // 2 - Dados pessoais [Title]
  private bsMode = new BehaviorSubject<number>(0)
  private bsTerminal = new BehaviorSubject<Terminal[]>([])
  private bsDep = new BehaviorSubject<Department_Filter[]>([])
  private bsHost = new BehaviorSubject<User[]>([])
  private bsPedido = new BehaviorSubject<Pedido[]>([]) 
  private sPedido_info = new Subject<PedidoInfo[]>()
  private sPedido_visitor = new Subject<UserVisitor[]>()
  private sPedido_extra = new Subject<ExtraVisit[]>()
  private bsVisit_Sarted = new BehaviorSubject<boolean>(false)
  private bsSearched = new Subject<string>()
  private bsIsFiltering = new BehaviorSubject<boolean>(false)
  private bsDashbData = new BehaviorSubject<any>(null)
  private bsLstExtra = new BehaviorSubject<Extra[]>([])
  private sTriggerRefresh = new Subject<boolean>()


  modeListener = this.bsMode.asObservable()
  terminalListener = this.bsTerminal.asObservable()
  depListener = this.bsDep.asObservable()
  hostListener = this.bsHost.asObservable()
  pedidoListener = this.bsPedido.asObservable()
  pedidoInfoListener = this.sPedido_info.asObservable()
  pedidoVisistoListener = this.sPedido_visitor.asObservable()
  pedidoExtraListener = this.sPedido_extra.asObservable()
  visitStartedListener = this.bsVisit_Sarted.asObservable()
  searchedListener = this.bsSearched.asObservable()
  isFilteringListerner = this.bsIsFiltering.asObservable()
  dashDataListener = this.bsDashbData.asObservable()
  lstExtraListener = this.bsLstExtra.asObservable()
  triggerRefreshListener = this.sTriggerRefresh.asObservable()

  //moment().format().split('T')[0]
  defDataStart:string = moment().format().split('T')[0] //moment().subtract(30, 'days').format().split('T')[0]
  defDataEnd:string = moment().add(14, 'days').format().split('T')[0] //moment().add(30, 'days').format().split('T')[0]

  loadingToastElement:HTMLIonToastElement = null
  
  curUser:User = this.authService.getUser()
 
  // socket = socketIo(environment.root_url)

  constructor(private http:HttpClient, 
              private alertController: AlertController, 
              private authService:AuthService,
              private toastController: ToastController) {
    this.authService.isAuth.subscribe(isAuth => {
      if (isAuth) {
        this.curUser = this.authService.getUser()
      } else {
        this.curUser = null
      }
    })
  }


  changeMode(mNum:number){
    this.bsMode.next(mNum)
  }

  triggerRefresh(val:boolean = true){
    this.sTriggerRefresh.next(val)
  }

  serverGetListTerminal(){
    let url = this.URL_VISITS+"/getTerminalPort"
    let parms = {'p_active':1}
    this.http.post<any>(url,parms).subscribe(tList => {
      const lTerm:Terminal[] = tList
      this.bsTerminal.next(lTerm)
    },err => {
      console.error(err)
      this.showAlert('Error Obtendo os dados, Tente novamente!')
      this.bsTerminal.next([])
    })
  }


  serverGetLisDep(cod_terminal:number){
    let url = this.URL_VISITS+"/getDepByTerm"
    let parms = {'p_id_comp':cod_terminal}
    this.http.post<any>(url,parms).subscribe(dList => {
      const lDep:Department_Filter[] = dList
      this.bsDep.next(lDep)
    }, err => {
      console.error(err)
      this.showAlert('Error Obtendo os dados, Tente novamente!')
      this.bsDep.next([])
    })
  }


  serverGetListUser(cod_terminal:number, cod_dep:number){
    let url = this.URL_VISITS+"/getUsersByDepAndTerm"
    let parms = { 'p_id_comp':cod_terminal,
                  'p_id_dep':cod_dep }

    this.http.post<any>(url,parms).subscribe(lUser =>{
      const uList:User[] = lUser
      this.bsHost.next(uList)

    }, err => {
      console.error(err)
      this.showAlert('Error Obtendo os dados, Tente novamente!')
      this.bsHost.next([])
    })
  }

// For all
  serverGetPedidos( p_dtm_start:string = this.defDataStart,
                    p_dtm_end:string = this.defDataEnd) {

    let url = this.URL_VISITS+'/getVisitsStateUserMini'

    //User
    let codUser = this.curUser.cod
    let cod_role = this.curUser.cod_role


    let core_cod = environment.core_cod
    let thread_cod = environment.thread_cod
    let host_cod = environment.host_cod

    var bodyParm = {}

    // HOST
    if (cod_role === host_cod){
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end,
        'p_id_host': codUser
      }
    } 
    // THREAD
    else if (cod_role === thread_cod){
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end,
        'p_id_comp': this.curUser.cod_company
      }
    } 
    // CORE + THREAD
    else {
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end
      }
    }
    this.http.post<any>(url,bodyParm).subscribe(dList => {
     
      // Order by date
      dList.sort(function(a, b) {
        // convert date object into number to resolve issue in typescript
        if (a.length){
          a = a[0]
        } else if (b.length){
          b = b[0]
        }
        return  +new Date(a.sv_visit_date) - +new Date(b.sv_visit_date);
      })
      const pList: Pedido[] = dList
      this.bsPedido.next(pList)
    }, err => {
      console.error(err)
      this.bsPedido.next([])
      this.showAlert('Error Obtendo os dados, Tente novamente!')
    })      
  }


  serverGetPedidoInfo(cod_pedido:number){
    let bodyParm = {
      'p_cod': cod_pedido
    }
    let url = this.URL_VISITS+'/getVisitInfo'
    this.http.post<any>(url,bodyParm).subscribe(pInfo => {
      const p_Info:PedidoInfo[] = pInfo
      this.sPedido_info.next(p_Info)
      this.serverGetPeidoVisitor(cod_pedido)
    }, err => {
      this.showAlert('Error Obtendo os dados, Tente novamente!')
      this.sPedido_info.next(null)
      console.error(err)
    })
  }


  serverGetPeidoVisitor(cod_pedido:number){
    let bodyParm = {
      'p_cod': cod_pedido
    }
    let url = this.URL_VISITS+'/getVisitVisitor'
    this.http.post<any>(url,bodyParm).subscribe(pVisitor => {
      const lVisitor:UserVisitor[] = pVisitor
      this.sPedido_visitor.next(lVisitor)
      this.serverGetPedidoExtra(cod_pedido)
    }, err => {
      this.sPedido_visitor.next([])
      console.error(err)
    })
  }

  
  serverGetPedidoExtra(cod_pedido:number){
    let bodyParm = {
      'p_int_id':cod_pedido
    }
    let url = this.URL_VISITS+'/getSvisitExtra'
    this.http.post<any>(url,bodyParm).subscribe(pExtra => {
      const lExtra:ExtraVisit[] = pExtra 
      this.sPedido_extra.next(lExtra)
    }, err => {
      this.sPedido_extra.next([])
      console.error(err)
    })
  }


  serverCheckVisitStarted(cod_svisit:number){
    let bodyPar = {
      'p_cod_svisit':cod_svisit
    }
    let url = this.URL_VISITS+'/checkVisitStarted'
    this.http.post<any>(url, bodyPar).subscribe(isStarted => {
      var is
      if (isStarted === 1){
        is = true 
      }else{
        is = false
      }
      this.bsVisit_Sarted.next(is)
    }, err => {
      this.bsVisit_Sarted.next(false)
      console.error(err)
    })
  }


  serverAddHost(cod_visit){
    let user = this.curUser
    let bodyPar = {
      'p_id_host': user.cod,
      'p_id':cod_visit
    }
    let url = this.URL_VISITS+'/edit_visit_host'
    this.http.post<any>(url, bodyPar).subscribe(dbResp => {
      var bodyParm = {
        'p_int': 0,
        'p_id_svisit': cod_visit,
        'p_id_htc':user.cod,
        'p_bool_htc':1,
        'p_dtm_htc':moment().format()
      }
      let url = this.URL_VISITS+'/editVstate_state_htc'
      this.http.post<any>(url, bodyParm).subscribe(dbResp => {
        console.log("done")
      },err => {
        console.log(err)
      })
    }, err => {
      console.log(err)
    })
  }


  serverCancelPedidoState(cod_visit:number, 
                          isCancel:number, 
                          cod_why:number=null,
                          cancel_why:string=null){
    this.showLoadingToast("Cancelando o pedido #"+ cod_visit +" . . .")
    let datetime = moment().format()
    let user = this.curUser
    let url
    var bodyParm = {
        'p_id_svisit':cod_visit ,
        'p_htc_cancel':isCancel ,
        'p_cancel_why': cancel_why ,
        'p_cod_why':cod_why,
        'p_when_cancel': datetime
    }
      // HOST
    if (user.cod_role === environment.host_cod){
      bodyParm['p_int'] = 0
    } else if (user.cod_role === environment.thread_cod){
      bodyParm['p_int'] = 1
    } else {
      bodyParm['p_int'] = 2
    }
      
    url = this.URL_VISITS+'/editVstate_state_htc_cancel'
    this.http.post<any>(url, bodyParm).subscribe(resp => {
      var msg:string
      if (this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      if (isCancel === 1){
        msg = "O pedido #"+cod_visit+" foi CANCELADO"
      } else{
        msg = "O pedido #"+cod_visit+" foi DESCANCELADO"
      }
      this.showToastAffterAction(msg)
      this.triggerRefresh(true)
      //this.serverGetPedidos()
    },err =>{
      console.log(err)
      if (this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      if (isCancel ===1 ){
        this.showToastAffterAction("Não foi possivel CANCELAR o pedido #"+cod_visit, false)
      }else{
        this.showToastAffterAction("Não foi possivel DESCANCELAR o pedido #"+cod_visit, false)
      }
      this.triggerRefresh(false)
      
    })
  }


  serverChangePedidoState(cod_visit:number, 
                          state:number=null, 
                          cod_why:number=null, 
                          state_why:string=null){
    let user = this.curUser
    var datetime = moment().format()
    
    var bodyParm = {
        'p_id_svisit': cod_visit,
        'p_id_htc':user.cod,
        'p_bool_htc':state,
        'p_cod_why':cod_why,
        'p_dtm_htc':datetime
      }
    if (state===0){
      bodyParm['p_why'] = state_why
    }
    let url = this.URL_VISITS+'/editVstate_state_htc'
    
    // HOST
    if (user.cod_role === environment.host_cod){
      bodyParm['p_int'] = 0
    } else if (user.cod_role === environment.thread_cod){
      bodyParm['p_int'] = 1
    } else {
      bodyParm['p_int'] = 2
    }

    var msg:string
    if (state){
      msg = "Aceitando o pedido #"+cod_visit+" . . ."
    } else{
      msg = "Recusando o pedido #"+cod_visit+" . . ."
    }
    
    this.showLoadingToast(msg)

    this.http.post<any>(url, bodyParm).subscribe(resp => {
      var msg:string
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      if (state){
        msg = "O pedido #"+cod_visit+" foi ACEITE"
      } else{
        msg = "O pedido #"+cod_visit+" foi RECUSADO"
      }
      this.showToastAffterAction(msg)
      this.triggerRefresh(true)
    },err =>{
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      console.log(err)
      if (state){
        this.showToastAffterAction("Não foi possivel ACEITAR o pedido #"+cod_visit)
      }else{
        this.showToastAffterAction("Não foi possivel RECUSAR o pedido #"+cod_visit)
      }
      this.triggerRefresh(false)
    })
  }


  serverChangeMaterial(cod_svisit, lstMaterial){
    var url
    var bodypar
    var count = 0
    this.showLoadingToast("Modificando o material . . .")

    for (let i = 0; i < lstMaterial.length; i++) {
      let mterial = lstMaterial[i]
      let state = 0
      // let isMaterial = 1

      if (mterial['bool_is_active']) {state = 1}
      // if(mterial['p_ismtrl']) {isMaterial = 1}

      bodypar = {
        'p_svisit':cod_svisit,
        'p_oname':mterial['str_oname'],
        'p_qnt':mterial['int_qnt'],
        'p_ismtrl':1,
        'p_is_active':state,
        'p_cod_user':this.curUser.cod,
        'p_dtm': moment().format()
      }
      if (mterial['int_id'] === null){
        url = this.URL_VISITS+"/addSvisitExtra"
      }else{
        url = this.URL_VISITS+"/editSvisitExtra_v2" 
        bodypar['p_id'] = mterial['int_id']
        bodypar['p_is_active'] = state
      }
      this.http.post<any>(url,bodypar).subscribe(dbResp =>{
        count += 1
        if (count === lstMaterial.length && this.loadingToastElement){
          this.loadingToastElement.dismiss()
          this.showToastAffterAction("Material modificado!")
          this.triggerRefresh(true)
        }
      }, err =>{
        count += 1
        console.log(err)
        if (count === lstMaterial.length && this.loadingToastElement){
          this.loadingToastElement.dismiss()
          this.showToastAffterAction("Material não modificado!", false)
          this.triggerRefresh(true)
        }
      })
    }
  }


  serverEditVisitSpec(p_cod, 
                      p_is_perido:Boolean = false,
                      p_periodo?:Number,
                      p_is_gate:Boolean = false,
                      p_id_gate?:Number){
    
    var bodyParm = {}
    if (p_is_perido){
      this.showLoadingToast("Alterando o periodo. . .")
      bodyParm  = {
        'p_is_periodo':p_is_perido,
        'p_periodo':p_periodo
      }
    } else if (p_is_gate){
      this.showLoadingToast("Alterando o portao. . .")
      bodyParm  = {
        'p_is_gate':p_is_gate,
        'p_id_gate':p_id_gate
      }
    } else {
      this.showLoadingToast("Alterando os dados. . .")
      bodyParm  = {
        'p_is_periodo':p_is_perido,
        'p_periodo':p_periodo,
        'p_is_gate':p_is_gate,
        'p_id_gate':p_id_gate
      }
    }
    
    let url = this.URL_VISITS+"/editVisitSpec"
     
    if(p_cod.length){
      bodyParm['p_cod'] = p_cod['cod'][0]
    } else {
      bodyParm['p_cod'] = p_cod['cod']
    }
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      var msg = "Dados Alterados!"
      this.showToastAffterAction(msg)
      this.triggerRefresh(true)
    }, err => {
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      console.log(err)
      var msg = "Não foi possivel alterar os dados."
      this.showToastAffterAction(msg, false)
      this.triggerRefresh(false)
    })
  }


  serverFilterData(data){
    let objData = data
    let p_dtm_start = objData['dtm_inicio']
    let p_dtm_end = objData['dtm_fim']

    let cod_term = objData['cod_ter']
    let cod_dep = objData['cod_dep']
    let cod_host = objData['cod_host']
    let tip_pedido = objData['tip_pedido']
    let tip_visit = objData['tip_visit']
    let tip_state = objData['tip_estado']

    //Informar que esta levar dados na db
    this.bsIsFiltering.next(true)
    //User
    var codUser = this.curUser['cod']
    var url:string
    var bodyParm = {}

    if (cod_host !== null){
      codUser = cod_host
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end,
        'p_id_host': codUser
      }
    } else if (cod_dep !== null){
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end,
        'p_id_comp': cod_term,
        'p_id_dep': cod_dep
      }
    }  else if (cod_term !== null){
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end,
        'p_id_comp': cod_term
      }
    } else{
      bodyParm = {
        'p_dtm_start': p_dtm_start,
        'p_dtm_end': p_dtm_end
      }
    }

    url = this.URL_VISITS+'/getVisitsStateUserMiniFiltrar'
    
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      var newData = this.flirterData(tip_visit, tip_pedido, tip_state, dbData)
      newData.sort(function(a, b) {
        // convert date object into number to resolve issue in typescript
        return  +new Date(a.sv_visit_date) - +new Date(b.sv_visit_date);
      })
      const lPedido:Pedido[] = newData
      this.bsPedido.next(lPedido)
      this.bsIsFiltering.next(false)
    },
    err => {
      this.bsPedido.next([])
      this.bsIsFiltering.next(false)
      this.showAlert("Não foi possivel obter os dados ")
      console.error('erro: ',err)
    })
  }


  serverGetDashboardData(){
    let aUser = this.curUser
    let bodyPar = { 'p_dtm_start':this.defDataStart ,
                    'p_dtm_end':this.defDataEnd
                  }

    if (aUser.cod_role === environment.host_cod){
      bodyPar['p_id_host'] = aUser.cod
    } else if (aUser.cod_role === environment.thread_cod) {
      bodyPar['p_id_comp'] = aUser.cod_company
    }

    let url = this.URL_VISITS+'/getMobileDashboard'
    this.http.post<any>(url, bodyPar).subscribe(dbData => {
      this.bsDashbData.next(dbData)
    }, err => {
      this.bsDashbData.next(null)
      console.log(err)
    })
  }


  serverGetExtras(cod){
    var etype_cod = cod
    var bodyParm = {'p_cod_etype': etype_cod}
    let url = this.URL_VISITS+"/getExtras"
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      const lEx:Extra[] = dbData
      this.bsLstExtra.next(lEx)
    }, err =>{
      this.bsLstExtra.next([])
      this.showToastAffterAction('Error obtendo os extras. Tente novamente', false)
      console.log(err)
    })
  }


  flirterData(tipVisit, tipPedido, tipEstado, dataIn){
    var newArr = dataIn
    // Tipo de vistia
    if (tipVisit === 1){
      newArr = dataIn.filter( pedido => {
        return !pedido.length
      })
    } else if (tipVisit === 2){
      newArr = dataIn.filter( pedido => {
        return pedido.length
      })
    }
    // Tipo de pedido
    if (tipPedido === 1){
      newArr = newArr.filter( pedido => {
        if (pedido.length){
          return pedido[0]['sv_is_access']
        }else {
          return pedido['sv_is_access']
        }
      })
    } else if (tipPedido === 2){
      newArr = newArr.filter( pedido => {
        if (pedido.length){
          return !pedido[0]['sv_is_access']
        }else {
          return !pedido['sv_is_access']
        }
      })
    }
    // Tipo de estado
    newArr = newArr.filter( pedido => {
      if (pedido.length){
        return pedido[0]['st_state'] === tipEstado || tipEstado === -2
      }else {
        return pedido['st_state'] === tipEstado || tipEstado === -2
      }
    })
    return newArr
  }


  searchaData(val:string){
    this.bsSearched.next(val)
  }


  async showAlert(msg:string) {
    this.alertController.create({
      message: msg,
      buttons: ['Ok']
    }).then( alert => alert.present())
  }


  async showLoadingToast(msg){
    this.toastController.create({
      message:msg,
      color:"warning",
      buttons: [{
        text: 'X',
        role: 'cancel'
      }]
    }).then(toast => {
      this.loadingToastElement = toast
      this.loadingToastElement.present()
    })
  }


  async showToastAffterAction(msg, gb= true) {
    if (gb){
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"success",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    } else {
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"danger",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    }
  }



}
