import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import {Plugins, CameraResultType, Capacitor, FilesystemDirectory, 
  CameraPhoto, CameraSource} from '@capacitor/core'
import { Photo } from '../top/photo';
import { Platform } from '@ionic/angular';
const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  private PHOTO_STORAGE: string ="ID_PHOTOS"
  // private photoS = new Subject<Photo>()
  // photoListener = this.photoS.asObservable()
  private platform: Platform

  constructor(platform: Platform) {
    this.platform = platform
   }


  async takePhoto(){
    return await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality:100,
    })
  }

  async loadSaved(){
    // Retrieve cached photo 
    const photoRetr = await Storage.get({key: this.PHOTO_STORAGE})
    let photo:Photo = JSON.parse(photoRetr.value) || null
    
    const readFile = await Filesystem.readFile({
      path:photo.filepath,
      directory: FilesystemDirectory.Data
    })
    photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`
    return photo;
  }

  genName(nid:string, front:String){
    return [nid,front].join("_")
  }


  async getImgFile(cameraPhoto:CameraPhoto){
    const response = await fetch(cameraPhoto.webPath!)
    return response.blob()
  }


  // async savePicture(cameraPhoto: CameraPhoto){
    
  //   // Convert photo to base64 format, required by FileSystem API to save 
  //   const base64Data = await this.readAsBase64(cameraPhoto)
  //   // Write the file to the data dir
  //   let fileName = new Date().getTime()+'.jpeg'
  //   const savedFile = await Filesystem.writeFile({
  //     path: fileName,
  //     data: base64Data,
  //     directory: FilesystemDirectory.Data
  //   })
  //   // Use webPath to display the new image instead of base64 since it's
  //   // already loaded into memory
  //   return {
  //     filepath: fileName,
  //     webviewPath: cameraPhoto.webPath
  //   };
  // }

  // async readAsBase64(cameraPhoto:CameraPhoto){
  //   // Getch the photo, read as a blob, then convert to base64 format
  //   const response = await fetch(cameraPhoto.webPath!)
  //   const blob = await response.blob()
  //   return await this.convertBlobToBase64(blob) as string;
  // }

  
  
  // convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
  //   const reader = new FileReader;
  //   reader.onerror = reject;
  //   reader.onload = () => {
  //       resolve(reader.result);
  //   };
  //   reader.readAsDataURL(blob);
  // });
}
