// import { Network } from '@ionic-native/network/ngx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../top/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private AUTH_URL = environment.root_url+"/auth"
  private ADMIN_URL = environment.root_url+"/admin"

  private user:User
  private isIsAuth:boolean = false

  private sIsAuth = new Subject<boolean>() 
  private sError = new Subject<string>()
  private sGate = new Subject<any>()
  private sLOut = new Subject<boolean>()

  isAuth = this.sIsAuth.asObservable()
  gateListener = this.sGate.asObservable()
  errListener = this.sError.asObservable()
  lOutListener = this.sLOut.asObservable()

  curGate:any = null



  loadingToastElement:HTMLIonToastElement = null

  constructor(private http: HttpClient, 
              private router: Router, 
              private alertController: AlertController,
              private navController: NavController,
              private toastController: ToastController,
              private plataform:Platform,
              private storage:Storage) { }

  logIn(contacto:string, psw:string){
    let bodyParams = {
      'p_isvisitor':0,
      'p_email_pnumber':contacto,
      'p_psw':psw
    }
    // USE FOR TESTING GATEKEEPR ON TRAILGATE_DB
    // this.user = this.getUser()
    // this.isIsAuth = true
    // this.sIsAuth.next(true)
    // this.redirectUser()
    console.log(this.AUTH_URL+'/login')
    this.http.post<any>(this.AUTH_URL+'/login',bodyParams).subscribe(dbResp => {
      this.user = dbResp
      console.log(dbResp)
      if (dbResp !== null){
        if (this.user.isactive){
          this.isIsAuth = true
          this.sIsAuth.next(true)
          this.redirectUser()
        }else{
          this.isIsAuth = false
          this.sIsAuth.next(false)
        }
      } else {
        this.isIsAuth = false
        this.sIsAuth.next(false)
      }
    }, err => {
      if(err.status === 0 ){
        this.sError.next("Erro de conexcão ao servidor. "+err.statusText)
      }
      this.isIsAuth = false
      this.sIsAuth.next(false)
      console.log(err)
    })
  }


  serverUpdateUserInfo(bodyParm, user:User){
    this.showLoadingToast("Editando. . .")
    let url = this.AUTH_URL+"/editUser_v2"
    this.http.post<any>(url, bodyParm).subscribe(dbResp =>{
      this.user = user
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      this.showToastAffterAction("Dados editados!", true)
    }, err =>{
      if(this.loadingToastElement){
        this.loadingToastElement.dismiss()
      }
      console.error(err)
      this.showToastAffterAction("Não foi possivel editar os teus dados.", false)
    })
  }


  getUser(){
    if (this.user){
      return JSON.parse(JSON.stringify(this.user))
    }
    console.log("no user")
    return null
    // return {
    //   'cod': 6, //9, 8, 15, 284
    //   'cod_company': 4,
    //   'cod_department': 9,
    //   'cod_role': 55, // 53, 52, 51, 55
    //   'cod_type_id': 50,
    //   'company': "MPDC",
    //   'department': "Segurança",
    //   'dtm_registry': "2020-01-02T20:20:20.000Z",
    //   'email': "Isac@gmail.com",
    //   'exp_date_id': "2021-02-02T00:00:00.000Z",
    //   'fname': "Isac",
    //   'isactive': true,
    //   'isvisitor': false,
    //   'lname': "Massale",
    //   'nationality': "Mozambique",
    //   'nid': "CASRR4FX",
    //   'pnumber_1': "847931875",
    //   'pnumber_2': "8479318744",
    //   'psw': "123456",
    //   'tokken': "h61JfXxkSA5Nv3uPOSders89iD0iXQ10",
    //   'type_id': "Outros",
    //   'user_role': "Gestor do departamento de segurança interna",
    // }
    
  }


  redirectUser(){
    if (this.user.cod_role === environment.gatekeeper_cod) {
      this.router.navigate(['gatekeeper','gates'])
      // if (this.plataform.is("tablet"||"ipad"||"hybrid")){
      //   this.router.navigate(['gatekeeper','gates'])
      // } else {
      //   this.showAlert("Porfavor utilize um Tablet ou iPad para entrar como Agente de segurança do portão.")
      // }
    } else if ((this.user.cod_role === environment.host_cod) ||
                (this.user.cod_role === environment.thread_cod) ||
                (this.user.cod_role === environment.core_cod)) {
      this.router.navigate(['htc','dashboard'])
      this.storage.set('user',this.user).then(() => {
        console.log("######### User "+this.user.cod+" stored")
      }, err => {
        console.log("######### Rejectedon Saving ")
        console.log(err)
      }).catch(err => {
        console.log("######### Error catchd on Saving ")
        console.log(err)
      })
    } else {
      this.showAlert("Usuario invalido! utilize o aplicativo web")
    }
  }

  GetStoredUser(){
    return this.storage.get('user')
  }

  removeStoredUser(){
    return this.storage.clear().then(val => {
      console.log("removed: ", val)
    })
  }


  setGate(key,name){
    this.curGate = {'key':key, 'name':name}
    this.sGate.next(this.curGate)
  }

  unSetGate(){
    this.sLOut.next(true)
    this.curGate = null
    this.sGate.next(null)
  }

  get getIsUserAuth(){
    return this.isIsAuth
  }

  getGate(){
    return this.curGate //{'key':key, 'name':name}
  }

  async LogOut(){
    await this.alertController.create({
      header:"Comfirmação",
      message: "Deseja mesmo sair ?",
      buttons: [{
        text: 'Não',
        role: 'cancel'
      },{
        text: 'Sim',
        handler: () => {
          this.sIsAuth.next(false)
          this.user = null
          this.unSetGate()
          this.removeStoredUser()
          this.navController.pop()
          this.router.navigate(['auth'])
        }
      }]
    }).then( alert => alert.present())
  }


  async showAlert(msg:string) {
    this.alertController.create({
      message: msg,
      buttons: ['Ok']
    }).then( alert => alert.present())
  }


  async showLoadingToast(msg){
    this.toastController.create({
      message:msg,
      color:"warning",
      buttons: [{
        text: 'X',
        role: 'cancel'
      }]
    }).then(toast => {
      this.loadingToastElement = toast
      this.loadingToastElement.present()
    })
  }


  async showToastAffterAction(msg, gb= true) {
    if (gb){
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"success",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    } else {
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"danger",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    }
  }
}
