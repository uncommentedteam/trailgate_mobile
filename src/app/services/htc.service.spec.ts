import { TestBed } from '@angular/core/testing';

import { HtcService } from './htc.service';

describe('HtcService', () => {
  let service: HtcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HtcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
