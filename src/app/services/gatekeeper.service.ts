import { Terminal } from './../top/terminal';
import { ExtraVisit } from './../top/extra_visit';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { AlertController, ToastController } from '@ionic/angular';
import { User } from '../top/user';
// import * as socketIo from 'socket.io-client'
import * as moment from 'moment';
import { PedidoIO } from '../top/PedidoIO';
import { Extra } from '../top/extra';
import { AuthService } from './auth.service';
import { CameraPhoto } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class GatekeeperService {
  // 0 -  Gate Selection [Gate]
  // 1 - Entradas/Saidas [Search with buttons]

  private bsMode = new BehaviorSubject<number>(2)
  private bsGates = new Subject<Extra[]>()
  private bsPedidosIo = new BehaviorSubject<PedidoIO[]>([])
  private sIsLoading = new Subject<boolean>()
  private sExtras = new Subject<ExtraVisit[]>()
  private sMPR = new Subject<Extra[]>()
  private sGetDataError = new Subject<boolean>()
  private sSearched = new Subject<string>()
  private sPosAproved = new Subject<PedidoIO>()
  private sIsEntranceTrigger = new Subject<boolean>()
  private sTriggerRefresh = new Subject<boolean>()
  private bsLstExtra = new BehaviorSubject<Extra[]>([])
  private bsTerminal = new BehaviorSubject<Terminal[]>([])
  private sPS = new Subject<boolean>()
  private bsVisitors = new BehaviorSubject<User[]>([])

  visitorsLis = this.bsVisitors.asObservable()
  psLis = this.sPS.asObservable()
  terminalListener = this.bsTerminal.asObservable()
  lstExtraListener = this.bsLstExtra.asObservable()
  modeListener = this.bsMode.asObservable()
  gatesListener = this.bsGates.asObservable()
  pedidosIoListener = this.bsPedidosIo.asObservable()
  isLoadingListener = this.sIsLoading.asObservable()
  extrasListener = this.sExtras.asObservable()
  mprListener = this.sMPR.asObservable()
  getDataError = this.sGetDataError.asObservable()
  searchedListener = this.sSearched.asObservable()
  posAprovedListener = this.sPosAproved.asObservable()
  EntraceTrigggerListener = this.sIsEntranceTrigger.asObservable()
  triggerRefreshListener = this.sTriggerRefresh.asObservable()
  
  URL_VISIT =  environment.root_url+"/visits"
  URL_AUTH = environment.root_url+"/auth"
  URL_ADMIN = environment.root_url+"/admin"

  loadingToastElement:HTMLIonToastElement = null

  curUser:User = this.authService.getUser()
  curGate:any = this.authService.getGate()
  hasGate:boolean = false

  private lstMPRE:Extra[] = null
  private lstMPRS:Extra[] = null
  
  // socket = socketIo(environment.root_url)
  private lstUsers:User[] = []

  constructor(private http:HttpClient, 
              private authService:AuthService, 
              private alertController: AlertController, 
              private toastController: ToastController) { 
    this.authService.isAuth.subscribe(isAuth => {
      if (isAuth) {
        this.curUser = this.authService.getUser()
      } else {
        this.curUser = null
      }
    })
  }

  changeMode(nNum:number){
    // 0 - GATE
    // 1 - IO
    // 2 - PS
    this.bsMode.next(nNum)
  }

  changeGate(obj){
    this.curGate = obj
    this.hasGate = true 
  }

  changeEntrance(isEntrance:boolean){
    this.sIsEntranceTrigger.next(isEntrance)
  }

  triggerRefresh(val:boolean = true){
    this.sTriggerRefresh.next(val)
  }
  
  triggerIsloading(is:boolean){
    this.sIsLoading.next(is)
  }

  serverGetGate(){
    // Check if it has it 
    var bodyParm = {'p_cod_etype': environment.gate_cod}
    let url = this.URL_VISIT+"/getExtras"
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      console.log(dbData)
      const lGat:Extra[] = dbData
      this.bsGates.next(lGat)
    }, err =>{
      this.bsGates.next([])
      this.showAlert('Error obtendo os dados dos portoes. Tente novamente')
      console.error(err)
    })
  }


  serverGetDataIO(isIn:boolean){
    let dataStart:string = moment().format()//moment().subtract(30, 'days').format()
    let dataEnd:string = moment().add(7, 'days').format()
    let bodyParm = {
      'p_dtm_start': dataStart.split('T')[0],
      'p_dtm_end': dataEnd.split('T')[0],
      'p_cod_gate': this.curGate['key']
    }
    let url = this.URL_VISIT
    if (isIn) {
      url +="/getVisitEntrace"
    } else {
      url +="/getVisitOut" 
    }
    this.http.post<any>(url,bodyParm).subscribe(dBack => {
      const lstData:PedidoIO[] =  this.orderPedidosByDate(dBack)
      this.bsPedidosIo.next(lstData)
      this.sGetDataError.next(false)
    }, err => {
      this.bsPedidosIo.next([])
      this.sGetDataError.next(true)
      console.error(err)
    })
  }


  serverGetPedidoExtra(cod:number){
    let bodyParm = {'p_int_id':cod}
    let url = this.URL_VISIT+'/getSvisitExtra'
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      const lExtras:ExtraVisit[] = dbData
      this.sExtras.next(lExtras)
    }, err =>{
      console.error(err)
      this.sExtras.next([])
      // this.pedido_visit_extra_Source.next([])
    })
  }


  serverGetMPR(isEntrance:boolean){
    var etype_cod
    var cont = true
    if (isEntrance){
      etype_cod = environment.mre_obs
      if (this.lstMPRE){
        this.sMPR.next(this.lstMPRE)
        cont = false
      }
    } else {
      etype_cod = environment.mrs_obs
      if (this.lstMPRS){
        this.sMPR.next(this.lstMPRS)
        cont = false
      }
    }
    if (cont){
      var bodyParm = {'p_cod_etype': etype_cod}
      let url = this.URL_VISIT+"/getExtras"
      this.http.post<any>(url, bodyParm).subscribe(dbData => {
        const lGat:Extra[] = dbData
        if (isEntrance) {this.lstMPRE = lGat} else {this.lstMPRS = lGat}
        this.sMPR.next(lGat)
      }, err =>{
        console.error(err)
        this.sMPR.next([])
      })
    }
    
  }


  serverChangeMaterial(cod_svisit, lstMaterial, showToast = true){
    var url
    var bodypar
    var count = 0
    if (showToast){
      this.showLoadingToast("Modificando o material . . .")
    }
    debugger
    for (let i = 0; i < lstMaterial.length; i++) {
      let mterial = lstMaterial[i]
      let state = 0
      let isMaterial = 0

      if (mterial['bool_is_active']) {state = 1}
      if(mterial['bool_ismtrl']) {isMaterial = 1}

      bodypar = {
        'p_svisit':cod_svisit,
        'p_oname':mterial['str_oname'],
        'p_qnt':mterial['int_qnt'],
        'p_ismtrl':isMaterial,
        'p_is_active':1,
        'p_cod_user':this.curUser.cod,
        'p_dtm': moment().format()
      }
      if (mterial['int_id'] === null){
        url = this.URL_VISIT+"/addSvisitExtra"
      }else{
        url = this.URL_VISIT+"/editSvisitExtra_v2" 
        bodypar['p_id'] = mterial['int_id']
        bodypar['p_is_active'] = state
      }
      this.http.post<any>(url,bodypar).subscribe(dbResp =>{
        count += 1
        if(showToast){
          if (count === lstMaterial.length && this.loadingToastElement){
            this.loadingToastElement.dismiss()
            this.showToastAffterAction("Material modificado!")
            this.triggerRefresh(true)
          }
        }
      }, err =>{
        count += 1
        console.log(err)
        if (showToast){
          if (count === lstMaterial.length && this.loadingToastElement){
            this.loadingToastElement.dismiss()
            this.showToastAffterAction("Material não modificado!", false)
            this.triggerRefresh(false)
          }
        }
      })
    }
  }


  serverGatekeeperIo(lstVisitorObj:any, state:number=null, isEntrance:boolean=true, cod_why:number=null){
    let url = this.URL_VISIT+"/editVisit"
    let msg:string
    let count:number = 0
    // Mostrar que esta a permetir a entrada
    if (this.loadingToastElement === null) {
      if (isEntrance && cod_why === null){
        state = 1
        msg = "Permitindo a entrada . . . "
      } else if (isEntrance && cod_why != null) {
        state = 0
        msg = "Recusando a entrada . . . "
      } else if (!isEntrance && cod_why === null) {
        state = 0
        msg = "Permitindo a saida . . . "
      } else {
        state = 1
        msg = "Recusando a saida . . . "
      }
      this.showLoadingToast(msg)
    }
    let visitorObj:PedidoIO[]
    if (!lstVisitorObj.length){
      visitorObj = [lstVisitorObj]
    } else {
      visitorObj = lstVisitorObj
    }
    for (let i = 0; i < visitorObj.length; i++) {
      let visitor = visitorObj[i]

      var bodyParm = {
        'p_id': visitor['visit_id'],
        'p_id_x': visitor['x_cod'],
        'p_id_gk': this.curUser.cod,
        'p_id_svisit': visitor['cod'],
        'p_dtm': moment().format(),
        'p_id_obs': cod_why,
        'p_isin':state,
        'p_id_gate':this.curGate['key'],
        'p_is_active':1,
        // For check
        'p_is_entrance':isEntrance,
        'p_period':visitor.sv_period

      }
      this.http.post<any>(url,bodyParm).subscribe(dbResp =>{

        count += 1
        // Informar quem foi que foi aprovado
        if ((isEntrance && cod_why === null) || (!isEntrance && cod_why === null)){
          this.sPosAproved.next(visitor)
        }

        if (count >= visitorObj.length && this.loadingToastElement !== null){
          this.loadingToastElement.dismiss()
          this.loadingToastElement = null
          var msg:string
          if (isEntrance && cod_why === null){
            msg = "Entrada Permitida !"
          } else if (isEntrance && cod_why != null) {
            msg = "Entrada Recusanda !"
          } else if (!isEntrance && cod_why === null) {
            msg = "Saida perminitida !"
          } else {
            msg = "Saida recusada !"
          }
          this.showToastAffterAction(msg)
          this.triggerRefresh(true)
        }
        
      }, err =>{
        count += 1
        if (count >= visitorObj.length && this.loadingToastElement !== null){
          this.loadingToastElement.dismiss()
          this.loadingToastElement = null
          var msg:string
          if (state === 1){
            msg = "Error permetindo acesso. Tente novamente"
          } else {
            msg = "Error recusando acesso. Tente novamente"
          }
          console.error(err)
          this.showToastAffterAction(msg, false)
          this.triggerRefresh(false)
        }
      })
    }

  }


 /* serverSetVisitDone(visit_cod:number){
    let bodParm = {'p_cod':visit_cod}
    let url = this.URL_VISIT+"/setIsDone"
    this.http.post<any>(url,bodParm).subscribe(msg => {
      console.log(msg)
    }, err => {
      console.error(err)
    })
  }*/

  serverGetListTerminal(p_active:boolean = null, p_isport:boolean = null, p_isps:boolean = null){
    let url = this.URL_VISIT+"/getTerminalWithSpecs"
    let parms =  {'p_active': this.convertBoolToBit(p_active),
                  'p_isport':this.convertBoolToBit(p_isport),
                  'p_isps':this.convertBoolToBit(p_isps)}
    this.http.post<any>(url,parms).subscribe(tList => {
      const lTerm:Terminal[] = tList
      this.bsTerminal.next(lTerm)
    },err => {
      console.error(err)
      this.showAlert('Error Obtendo os dados, Tente novamente!')
      this.bsTerminal.next([])
    })
  }

  serverGetExtras(cod){
    var etype_cod = cod
    var bodyParm = {'p_cod_etype': etype_cod}
    let url = this.URL_VISIT+"/getExtras"
    this.http.post<any>(url, bodyParm).subscribe(dbData => {
      const lEx:Extra[] = dbData
      this.bsLstExtra.next(lEx)
    }, err =>{
      this.bsLstExtra.next([])
      this.showToastAffterAction('Error obtendo os dados. Tente novamente', false)
      console.log(err)
    })
  }


  
  serverCreatePublicServices(bodyParam) : Observable<any>{
    let url = this.URL_AUTH + '/create_PublicService'
    return this.http.post<any>(url, bodyParam)
  }


  serverGetAllVisitor(){
    let bodParm = {'p_isvisitor':1}
    let url = this.URL_ADMIN+"/getAllUsers"
    this.http.post<any>(url,bodParm).subscribe(dbData => {
      this.bsVisitors.next(dbData)
      this.lstUsers = dbData
    }, err => {
      console.error(err)
      this.bsVisitors.next([])
    })
  }

  serverNotifyUser(id_svisit:number): Observable<any>{
    let bodyParm = {
      "p_id_svisit":id_svisit,
      "p_id_from":this.curUser.cod
    }
    let url = this.URL_VISIT+"/notifyUser"
    return this.http.post(url, bodyParm)

  }


  get listAllUsers(){
    return  this.lstUsers

  }
  convertBoolToBit(value:boolean){
    if (value){
      return 1
    } else {
      if (value === null){
        return null
      } else {
        return 0
      }
    }
  }

  

  searchaData(val:string){
    this.sSearched.next(val)
  }


  resetPedidoIo(){
    this.bsPedidosIo.next([])
  }


  orderPedidosByDate(uoPedidos){
    let lstData = uoPedidos
    lstData.sort(function(a, b) {
      // convert date object into number to resolve issue in typescript
      if (a.length ){
      a = a[0]
      } 
      if (b.length){
      b = b[0]
      }
      return  +new Date(a.sv_visit_date) - +new Date(b.sv_visit_date);
      })
    return lstData
  }

  helpGetServertIn(dBack){
    
  }


  helpGetServertOut(dBack){
    
  }


  async showAlert(msg:string) {
    this.alertController.create({
      message: msg,
      buttons: ['Ok']
    }).then( alert => alert.present())
  }


  async showLoadingToast(msg){
    this.toastController.create({
      message:msg,
      animated:true,
      color:"warning",
      buttons: [{
        text: 'X',
        role: 'cancel'
      }]
    }).then(toast => {
      this.loadingToastElement = toast
      this.loadingToastElement.present()
    })
  }


  async showToastAffterAction(msg, gb= true) {
    if (gb){
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"success",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    } else {
      this.toastController.create({
        message: msg,
        duration: 2000,
        color:"danger",
        buttons: [{
          text: 'X',
          role: 'cancel'
        }]
      }).then(toast => toast.present())
    }
  }

}
