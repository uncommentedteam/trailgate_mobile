export interface User {
    cod?: number;
    fname?:string;
    lname?:string; 
    nationality?:string;
    cod_type_id?: number; 
    type_id?:string;
    nid?:string;
    exp_date_id?:string; 
    email?:string; 
    pnumber_1?:string; 
    pnumber_2?:string; 
    psw?:string; 
    isvisitor?:boolean; 
    isactive?:boolean; 
    cod_company?: number; 
    company?:string; 
    cod_department?: number;
    department?:string; 
    cod_role?: number; 
    user_role?:string; 
    dtm_registry?:string; 
    tokken?:string;
    cod_gate?:number;
    gate?:string;
}
