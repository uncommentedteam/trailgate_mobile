export interface Pedido {
    cod?: number;
    st_core_cod_role?:  number;
    st_core_id?: number;
    st_core_is?: boolean;
    st_core_is_cancel?: boolean;
    st_host_cod?: number;
    st_host_cod_role?: number;
    st_host_is?: boolean
    st_host_is_cancel?: boolean;
    st_state?: number; //-1 Rejected // 0 Pendente // 1 Aprovado // 2 Cancelado
    st_threa_cod?: number;
    st_threa_is?: boolean;
    st_threa_is_cancel?: boolean;
    st_thread_cod_role?: number;
    sv_code?: string;
    sv_is_access?: boolean;
    sv_visit_date?: string;
    sv_visit_dateTime?: string;
    sv_visit_date_date?: string;
    sv_visit_date_time?: string;
    x_cod?: number;
    x_fname?: string;
    x_isvitor?: boolean;
    x_lname?: string;
    sv_is_done?:boolean;
    sv_is_ps?:boolean;
}