import { ExtraVisit } from "./extra_visit";

export function extractParametersForPublicService(
    cod_gate: number,
    cod_gk: number,
    fname:string,
    lname:string,
    ntel:string, 
    cod_tipoId:any,
    nid:string,
    cod_moto:any,
    cod_dest:any,
    p_cod_visitor?:any,
    lImgName?:string[], 
    lImage?:Blob[]|File[]): object|FormData {
        let bodyParam = null
        if (lImage[0]) {
            bodyParam = new FormData()
            bodyParam.append('p_fname', fname)
            bodyParam.append('p_lname', lname)
            bodyParam.append('p_pnumber_1', ntel)
            bodyParam.append('p_nid', nid)
            bodyParam.append('p_id_type_id', String(cod_tipoId))
            bodyParam.append('p_cod_company', String(cod_dest))
            bodyParam.append('p_cod_gate', String(cod_gate))
            bodyParam.append('p_cod_rpp', String(cod_moto))
            bodyParam.append('p_cod_gk', String(cod_gk))
            if (p_cod_visitor !== null){
                bodyParam.append('p_cod_visitor', String(p_cod_visitor))
            } 
            for (let i = 0; i < lImgName.length; i++) {
                const imgName = lImgName[i];
                const image = lImage[i]
                if (image !== null){
                  bodyParam.append("img", image, imgName)
                }
            } 
        } else {
            bodyParam = {
                'p_fname':fname,
                'p_lname':lname,
                'p_pnumber_1':ntel,
                'p_nid':nid,
                'p_id_type_id':cod_tipoId,
                'p_cod_company':cod_dest,
                'p_cod_gate':cod_gate,
                'p_cod_rpp':cod_moto,
                'p_cod_gk':cod_gk,
                'p_cod_visitor':p_cod_visitor,
              }
              if (p_cod_visitor !== null){
                bodyParam['p_cod_visitor'] = p_cod_visitor
              } 
        }
    return bodyParam

}