import { ExtraVisit } from './extra_visit';
import { Pedido } from './pedido';
export class Methods {
    
  searchPedido(val:string, listPedidos){
    var newlistPedidos = listPedidos.filter( (item) => {
      if (item.length){
        for (let i in item){
          var fname = item[i]['x_fname'].toLocaleLowerCase()
          var lname = item[i]['x_lname'].toLocaleLowerCase()
          var code = item[i]['sv_code']
          if (code === null){
            code = ' '
          } else if (lname === null){
            lname = ' '
          } else if (fname === null){
            fname = ' '
          }
          code = code.toLocaleLowerCase()
          var fullName = fname+' '+lname
          try{
            if (fname.indexOf(val.toLocaleLowerCase()) > -1||
            lname.indexOf(val.toLocaleLowerCase()) > -1||
            fullName.indexOf(val.toLocaleLowerCase()) > -1||
            code.indexOf(val.toLocaleLowerCase()) > -1){
            return item  
            }
          }catch (err){
            console.log(err)
            console.log(item[i])
          }
        }
      }else{
        var fname= item['x_fname'].toLocaleLowerCase()
        var lname= item['x_lname'].toLocaleLowerCase()
        var code = item['sv_code']
        if (code === null){
          code = ' '
        } else if (lname === null){
          lname = ' '
        } else if (fname === null){
          fname = ' '
        }
        code = code.toLocaleLowerCase()
        var fullName = fname+' '+lname
        try {
          if (fname.indexOf(val.toLocaleLowerCase()) > -1||
          lname.indexOf(val.toLocaleLowerCase()) > -1||
          fullName.indexOf(val.toLocaleLowerCase()) > -1||
          code.indexOf(val.toLocaleLowerCase()) > -1)
          return item 
        } catch (err){
          console.log(err)
          console.log(item)
        }
      }
    })
    console.log(newlistPedidos)
    return(newlistPedidos)
  }

  getRandomInt() {
    let min = Math.ceil(0);
    let max = Math.floor(1000);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  split_Extras(lExtras){
    let lMaterial:ExtraVisit[] = []
    let lMatricula:ExtraVisit[] = []
    for (let obj of lExtras){
      if (obj.bool_ismtrl){
        lMaterial.push(obj)
      } else {
        lMatricula.push(obj)
      }
    }
    return [lMaterial, lMatricula]
  }


}
