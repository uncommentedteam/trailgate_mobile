export interface UserVisitor {
    cod_entity: number;
    cod_provenance: number;
    email: string;
    entity: string;
    fname: string;
    int_id_x: number;
    lname: string;
    nationality: string;
    nid: string;
    pnumber_1: string;
    provenance: null
    type_id: string;
}