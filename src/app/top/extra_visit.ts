export interface ExtraVisit{
    bool_is_active: boolean;
    bool_ismtrl: boolean;
    int_id: number;
    int_qnt: number;
    str_oname: string;
}