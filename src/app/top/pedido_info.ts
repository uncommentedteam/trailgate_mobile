export interface PedidoInfo {
    cod: number;
    st_host_cod: number;
    st_host_fname: string;
    st_host_lname: string;
    sv_cod_comp: number;
    sv_cod_dep: number;
    sv_cod_gate: number;
    sv_cod_rrp: number;
    sv_cod_sub_company: number;
    sv_company: string;
    sv_department: string;
    sv_detail: string;
    sv_gate: string;
    sv_is_access: boolean;
    sv_is_company: boolean;
    sv_is_internal: boolean;
    sv_is_multi_entry: boolean;
    sv_period: number;
    sv_rrp: string;
    sv_sub_company:string;
    sv_visit_date: string;
}
