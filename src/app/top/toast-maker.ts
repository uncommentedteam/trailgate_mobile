import { ToastController } from "@ionic/angular";

export class ToastMaker {

    private static instance:ToastMaker

    constructor(private toastController: ToastController){ }

    public static getInstance(): ToastMaker {
        if (!ToastMaker.instance) {
            ToastMaker.instance = new ToastMaker(new ToastController)
        }
        return ToastMaker.instance
    }

    public async showLoadingToast(msg:string) {
        return await this.toastController.create({
            message:msg,
            animated:true,
            color:"warning",
            buttons: [{
              text: 'X',
              role: 'cancel'
            }]
        })
    }

    public async showToastAffterAction(msg:string, gb=true) {
        if (gb){
          this.toastController.create({
            message: msg,
            duration: 2000,
            color:"success",
            buttons: [{
              text: 'X',
              role: 'cancel'
            }]
          }).then(toaster => toaster.present())
        } else {
          this.toastController.create({
            message: msg,
            duration: 2000,
            color:"danger",
            buttons: [{
              text: 'X',
              role: 'cancel'
            }]
          }).then(toaster => toaster.present())
        }
      }
    
}
