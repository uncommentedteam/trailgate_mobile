export interface Terminal {
    int_id?: number;
    str_cname?: string;
    str_email?: string;
    str_pnumber?: string;
    str_pnumber_2?: string;
    str_psw?: string;
    bool_port?: boolean;
    bool_active?: boolean;
    int_id_user?: number;
}