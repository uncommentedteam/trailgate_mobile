export class Extra {
    active?: Boolean
    cod?: number
    cod_etype?: string
    etype?: string
    extra?: string
}