import { IoComponent } from './../gatekeeper/io/io.component';
import { GatesComponent } from './../gatekeeper/gates/gates.component';
import { ModalPedidoComponent } from './../modal/modal-pedido/modal-pedido.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AuthComponent } from '../auth/auth/auth.component';
import { ModalFilterComponent } from '../modal/modal-filter/modal-filter.component';
import { PedidoComponent } from '../htc/pedido/pedido.component';
import { ModalMaterialComponent } from '../modal/modal-material/modal-material.component';
import { DashboardComponent } from '../htc/dashboard/dashboard.component';
import { DadosComponent } from '../htc/dados/dados.component';
import { ModalPedidoIoComponent } from '../modal/modal-pedido-io/modal-pedido-io.component';
import { PsComponent } from '../gatekeeper/ps/ps.component';
import { ModalPesquisarComponent } from '../modal/modal-pesquisar/modal-pesquisar.component';

@NgModule({
  declarations: [AuthComponent, 
                  PedidoComponent,
                  DashboardComponent, 
                  DadosComponent,
                  GatesComponent,
                  IoComponent,
                  PsComponent,
                  ModalFilterComponent, 
                  ModalPedidoComponent, 
                  ModalMaterialComponent,
                  ModalPedidoIoComponent,
                  ModalPesquisarComponent
                ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  entryComponents: [ModalFilterComponent, 
                    ModalPedidoComponent, 
                    ModalMaterialComponent,
                    ModalPedidoIoComponent,
                    ModalPesquisarComponent,]
  
})
export class TopModule { }
