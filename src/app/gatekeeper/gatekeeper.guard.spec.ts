import { TestBed } from '@angular/core/testing';

import { GatekeeperGuard } from './gatekeeper.guard';

describe('GatekeeperGuard', () => {
  let guard: GatekeeperGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GatekeeperGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
