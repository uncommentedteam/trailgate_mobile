import { GatekeeperGuard } from './gatekeeper.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GatekeeperPage } from './gatekeeper.page';
import { GatesComponent } from './gates/gates.component';
import { IoComponent } from './io/io.component';
import { PsComponent } from './ps/ps.component';

const routes: Routes = [
  {
    path: '',
    component: GatekeeperPage,
    children:[{
      path:'gates',
      component:GatesComponent
    },

    {
      path: 'ps',
      component:PsComponent,
      canLoad:[GatekeeperGuard]
    },
    
    {
      path:'io/:isIn',
      component:IoComponent,
      canLoad:[GatekeeperGuard]
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GatekeeperPageRoutingModule {}
