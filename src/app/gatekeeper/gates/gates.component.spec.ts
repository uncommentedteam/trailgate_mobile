import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GatesComponent } from './gates.component';

describe('GatesComponent', () => {
  let component: GatesComponent;
  let fixture: ComponentFixture<GatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
