import { Subscription } from 'rxjs';
import { User } from 'src/app/top/user';
import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Extra } from 'src/app/top/extra';
import { AuthService } from 'src/app/services/auth.service';
import { GatekeeperService } from 'src/app/services/gatekeeper.service';


@Component({
  selector: 'app-gates',
  templateUrl: './gates.component.html',
  styleUrls: ['./gates.component.scss'],
})
export class GatesComponent implements OnInit {

  user:User
  loading:HTMLIonLoadingElement = null
  isLoading:boolean = false
  lstGate:Extra[] = []
  gateListener:Subscription 
  lOutListener:Subscription
  curGate = null

  constructor(private alertController: AlertController, 
              private loadingController: LoadingController,
              private authService: AuthService,
              private gkService:GatekeeperService) { }

              
  ionViewWillEnter(){
    this.gkService.changeMode(0)
  }
      
  
  ngOnInit() {
    this.showLoading()
    this.gateListener = this.gkService.gatesListener.subscribe(lGates=> {
      this.user = this.gkService.curUser
      // this.curGate = this.gkService.curGate
      this.isLoading = false
      if (this.loading){
        this.loading.dismiss()
      } 
      this.lstGate = lGates
      this.showAlertGate()
      
    })
    this.lOutListener = this.authService.lOutListener.subscribe(logOut => {
      if (logOut){
        this.curGate = null
      }
    })
  }


  onClick(){
    this.showAlertGate()
  }

  ngOnDestroy() {
    this.gateListener.unsubscribe()
    this.lOutListener.unsubscribe()
    this.curGate = null
  }

  showAlertGate(){
    let lstRb = []
    for (let index = 0; index < this.lstGate.length; index++) {
      const extra:Extra = this.lstGate[index]
      const input = {
        name: extra.cod,
        type:'radio',
        label:extra.extra,
        value:index,
        checked:false
      } 
      lstRb.push(input)
    }
    lstRb[0]['checked'] = true
    this.alertController.create({
      header: 'Selecione o portão',
      inputs: lstRb,
      buttons: [{
        text:'Cancelar',
        role:'cancel',
        cssClass:'danger',
        handler: () => {}
      },{
        text:'Ok',
        cssClass:'success',
        handler: data => {
          const index = data
          const extra:Extra = this.lstGate[index]
          this.authService.setGate(extra.cod, extra.extra)
          this.gkService.changeGate({'key':extra.cod, 'name':extra.extra})
          this.curGate = {'key':extra.cod, 'name':extra.extra}
        }
      }
    ]
    }).then(alert => {
      alert.present()
      alert.onDidDismiss()
    })
  }


  async showLoading(){
    this.loading = await this.loadingController.create({
      spinner: "crescent",
      message: "Carregando . . .",
      backdropDismiss: false
    })
    this.isLoading = true
    this.gkService.serverGetGate()
    this.loading.present()
  }
}