import { ToastMaker } from './../../top/toast-maker';
import { environment } from 'src/environments/environment';
import { PhotoService } from './../../services/photo.service';
import { NgForm } from '@angular/forms';
import { Extra } from './../../top/extra';
import { Terminal } from 'src/app/top/terminal';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GatekeeperService } from 'src/app/services/gatekeeper.service';
import { Subscription } from 'rxjs';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalPesquisarComponent } from 'src/app/modal/modal-pesquisar/modal-pesquisar.component';
import { User } from 'src/app/top/user';
import { ModalMaterialComponent } from 'src/app/modal/modal-material/modal-material.component';
import { ExtraVisit } from 'src/app/top/extra_visit';
import {extractParametersForPublicService} from "../../top/extractor"

@Component({
  selector: 'app-ps',
  templateUrl: './ps.component.html',
  styleUrls: ['./ps.component.scss'],
})
export class PsComponent implements OnInit {

  constructor(private gkService:GatekeeperService,
              private photService:PhotoService,
              private modalCrtl: ModalController,
              private alertController: AlertController) { }

  lstTipId:Extra[] = []
  lstMoto:Extra[] = []  
  lstDestino:Terminal[] = []

  motoCode = environment.rpp_cod
  tipIdCode = environment.tid_cod

  frontImgBlob:Blob = null
  backImgBlob:Blob = null

  imgFront = null
  imgBack = null

  extaSUB:Subscription
  terSUB:Subscription

  // ps: Subscription

  dictSelect= {}
  
  initSelectValue = 0

  error: any;

  @ViewChild('psForm') psForm: NgForm

  nullUser:User = {
    cod:null,
    fname:"",
    lname:"",
    cod_type_id:0,
    pnumber_1: "",
    nid:""
  } 

  lstMaterial:ExtraVisit[] = []

  userSelected:User = this.nullUser

  ngOnInit() {
    this.extaSUB = this.gkService.lstExtraListener.subscribe(lExtra => {
      if (lExtra.length > 0){
        if (lExtra[0].cod_etype === this.motoCode){
          this.lstMoto = lExtra
        } else {
          this.lstTipId = lExtra
        }
      }
    })

    this.terSUB = this.gkService.terminalListener.subscribe(lTer => this.lstDestino = lTer)
    
    if (this.psForm) {
      this.onNovoClicked()
    }
  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.extaSUB.unsubscribe()
    this.terSUB.unsubscribe()
    // this.ps.unsubscribe()
  }

  ionViewWillEnter() {
    this.gkService.changeMode(2)
    this.gkService.serverGetExtras(this.motoCode)
    this.gkService.serverGetExtras(this.tipIdCode)
    this.gkService.serverGetListTerminal(true, null, true)
  }

  // GET LIST OF ALL TYPE OF ID
  // GET LIST EMPRESAS DE SERVICO PUBLICK
  // GET LIST MOTIVO

  onSubmit(form: NgForm){
    // VERIFICA SE TEM OS DADOS BASE PARA CRIAR UMA VISITA
    if (form.valid){
      var toastMakerInstance = ToastMaker.getInstance()
      var loadingToast: HTMLIonToastElement = null
      try{
        
        const cod_gate = this.gkService.curGate['key']
        const cod_gk = this.gkService.curUser.cod
        const fname = form.value['fname']
        const lname = form.value['lname']
        const ntel = ('+258') + form.value['ntel']
        const nid = form.value['nid']
        const codTipId = form.value['tipId']
        const codMoto = form.value['motoId']
        const codTerm = form.value['destId']
        const codVisitor = this.userSelected.cod
        const imgFrontName = this.photService.genName(nid, "front")
        const imgBackName = this.photService.genName(nid, "back")

        var msg = "Criando acesso para "+fname+" "+lname+"..."

        toastMakerInstance.showLoadingToast(msg).then(_loadingToast => {
            loadingToast = _loadingToast
            loadingToast.present()
          }
        )
        
        let bodyParam = extractParametersForPublicService(
          cod_gate,
          cod_gk,
          fname,
          lname,
          ntel,
          codTipId,
          nid,
          codMoto,
          codTerm,
          codVisitor,
          [imgFrontName,imgBackName],
          [this.frontImgBlob, this.backImgBlob]
        )

        this.gkService.serverCreatePublicServices(bodyParam).subscribe( dbResponse => {
          if (loadingToast !== null) {
            loadingToast.dismiss()
            loadingToast = null
          }
          
          if (dbResponse.length > 0) {
            const status = dbResponse[0]['status']
            if (status === 1){
              toastMakerInstance.showToastAffterAction("Acesso Criado", true)
              if (this.lstMaterial){
                const svisit = dbResponse[0]['svisit']
                this.gkService.serverChangeMaterial(svisit, this.lstMaterial, false)
              }
              this.onNovoClicked()
            } else {
              console.log(dbResponse)
              toastMakerInstance.showToastAffterAction("Não foi possivel criar o acesso.", false)
            }
          } else { //When it fails 
            console.log(dbResponse)
            toastMakerInstance.showToastAffterAction("Não foi possivel criar o acesso.", false)
          }
        }, error => {
          this.error = error
          loadingToast.dismiss()
          toastMakerInstance.showToastAffterAction("Não foi possivel criar o acesso.", false)
        } )
      } catch (error){
        if (loadingToast !== null) {
          loadingToast.dismiss()
          loadingToast = null
        }
        this.gkService.showAlert("Error extraindo os dados, certifique que todos dados foram preenchidos bem. Tente novamente!")
        console.log(error)
      }
    } 
  }


  private _takePhoto(isFront:boolean){
    this.photService.takePhoto().then(photoObj => {
      console.log(photoObj)
      this.photService.getImgFile(photoObj).then(blob => {
        if (isFront){
          this.imgFront = photoObj.webPath
          this.frontImgBlob = blob
        } else {
          this.imgBack = photoObj.webPath
          this.backImgBlob = blob
        }
      })
    })
  }

  async presentAlertConfirmTakePhoto(isFront: boolean) {
    let side = "traseira"
    if (isFront) {
      side = "frontal"
    }
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Deseja tirar a foto da parte <strong>${side}</strong> do documento de indentificação!`,
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Sim',
          handler: () => {
            this._takePhoto(isFront)
          }
        }
      ]
    });
  
    await alert.present();
  }

  onMaterialClicked(){
    this.modalCrtl.create({
      component:ModalMaterialComponent,
      componentProps:{'lstMaterial':JSON.parse(JSON.stringify(this.lstMaterial))}
    }).then(
      modal => {
        modal.present()
        modal.onDidDismiss().then(value =>{
          console.log(value.data)
          const lMtrl:ExtraVisit[] = value.data['data']
          const isDone:boolean = value.data['isDone']
          if (lMtrl.length > 0 && isDone){
            this.lstMaterial = lMtrl
          }
        })
      }
    )
  }

  onNovoClicked(){
    this.psForm.resetForm()
    this.userSelected = this.nullUser
    this.lstMaterial = []
    this.frontImgBlob = null
    this.backImgBlob = null
    this.imgFront = null
    this.imgBack = null
  }
  
  onPesquisarClicked(){
    this.modalCrtl.create({
      component: ModalPesquisarComponent,
      backdropDismiss:false
    }).then( pComponet => {
      pComponet.present()
      pComponet.onDidDismiss().then(data => {
        if (data.data !==null){
          this.userSelected = data.data
        }
      })
    })
  }
}
