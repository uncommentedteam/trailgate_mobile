import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GatekeeperPageRoutingModule } from './gatekeeper-routing.module';

import { GatekeeperPage } from './gatekeeper.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GatekeeperPageRoutingModule
  ],
  declarations: [GatekeeperPage]
})
export class GatekeeperPageModule {}
