import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IoComponent } from './io.component';

describe('IoComponent', () => {
  let component: IoComponent;
  let fixture: ComponentFixture<IoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
