import { Subscription } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PedidoIO } from 'src/app/top/PedidoIO';
import { ModalController, Platform } from '@ionic/angular';
import { ModalPedidoIoComponent } from 'src/app/modal/modal-pedido-io/modal-pedido-io.component';
import { Methods } from 'src/app/top/methods';
import { GatekeeperService } from 'src/app/services/gatekeeper.service';

@Component({
  selector: 'app-io',
  templateUrl: './io.component.html',
  styleUrls: ['./io.component.scss'],
})
export class IoComponent implements OnInit {

  isInitLoad:boolean = false
  isLoading:boolean = false
  isRefreshing:boolean = false
  isIn:string
  isEntrance:boolean = true
  listPedidos:PedidoIO[] = []
  pedidosListener:Subscription
  searchListner:Subscription
  isEntranceListener:Subscription
  triggerRefreshSubs:Subscription
  refresher = null
  lstPedidosBackup:PedidoIO[] = []
  methods = new Methods
  curGate:any
  isIos:boolean

  constructor( private gkService:GatekeeperService, 
              private modalController: ModalController,
              private plataform:Platform,
              private router: ActivatedRoute) { }

  ionViewWillEnter() {
    if (this.plataform.is("ipad" || "iphone" || "ios")){
      this.isIos = true 
    } else {
      this.isIos = false
    }
    
    this.router.paramMap.subscribe((paramMap:ParamMap) => {
      if (paramMap.has('isIn')){
        if (paramMap.get('isIn') ==='1'){
          this.isEntrance = true
          this.gkService.changeEntrance(true)
          console.log('is in')
        }else{
          console.log('is Out')
          this.isEntrance = false
          this.gkService.changeEntrance(false)
        }
      }
    })

    this.gkService.resetPedidoIo()
    this.isInitLoad = true
    this.isLoading = true
    this.gkService.triggerIsloading(true)
    this.gkService.changeMode(1)
    this.gkService.serverGetDataIO(this.isEntrance)
  }

  ngOnInit() {

    this.isEntranceListener = this.gkService.EntraceTrigggerListener.subscribe(isEntrance => {
      this.isEntrance = isEntrance
    }
    )
    
    this.curGate = this.gkService.curGate

    // this.gkService.socket.on('update', () => {
    //   this.isLoading = true
    //   this.gkService.serverGetDataIO(this.isEntrance)
    // })
    this.triggerRefreshSubs = this.gkService.triggerRefreshListener.subscribe(triggered => {
      if (triggered){
        this.isLoading = true
        this.gkService.serverGetDataIO(this.isEntrance)
      }else{
        console.log("Something Happen")
      }
    })

    this.pedidosListener = this.gkService.pedidosIoListener.subscribe(lPedidos => {
      // HERE
      this.lstPedidosBackup = lPedidos
      this.listPedidos = lPedidos
      this.isInitLoad = false
      this.isLoading = false
      this.gkService.triggerIsloading(false)
      if (this.refresher){
        this.isRefreshing = false
        this.refresher.complete()
      }
    })

    this.searchListner = this.gkService.searchedListener.subscribe(val => {
      if (val != ''){
        this.listPedidos = this.methods.searchPedido(val, this.lstPedidosBackup)
      } else {
        this.listPedidos = this.lstPedidosBackup
      }
    }) 
  }


  onRefresher(event){
    this.isRefreshing = true
   this.gkService.serverGetDataIO(this.isEntrance)
    this.refresher = event.target
  }


  itemHeightFn(item, index) {
    return 130
  }

  itemHeightFnx(item, index) {
    return 150
  }

  trackByFn(index:number, item:any){
    if (item.length){ 
      return item[0].cod
    } else 
      return item.cod
  }  


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.listPedidos = []
    this.pedidosListener.unsubscribe()
    this.searchListner.unsubscribe()
  }


  setStatusText(visitorObj){
    var status
    if (visitorObj.length){
      status = visitorObj[0]['st_state']
    } else {
      status = visitorObj['st_state']
    }
    if (status === -1){
      return ["REJEITADO",status]
    } else if (status === 0){
      return ["PENDENTE",status]
    }else if (status === 1){
      return ["APROVADO",status]
    } else if (status === 2){
      return ["CANCELADO",status]
    }
  }

  onItemClick(visitorObj){
    this.modalController.create({
      component: ModalPedidoIoComponent,
      componentProps: {
                        'lstObjVisitor': visitorObj,
                        'isEntrance': this.isEntrance
                      },
      backdropDismiss:false,
      cssClass:'modal-pedido-io'
    }).then(modal => modal.present())
  }


  trackBy(index, item){
    return item
  }

}
