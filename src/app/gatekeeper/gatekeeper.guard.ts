
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GatekeeperService } from '../services/gatekeeper.service';

@Injectable({
  providedIn: 'root'
})
export class GatekeeperGuard implements CanLoad {
  constructor(private gkService: GatekeeperService, private router:Router){}
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.gkService.hasGate){
      this.router.navigate(['gatekeeper','gates'])
    } else {
      return this.gkService.hasGate
    }
  }
}
