import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GatekeeperService } from '../services/gatekeeper.service';

@Component({
  selector: 'app-gatekeeper',
  templateUrl: './gatekeeper.page.html',
  styleUrls: ['./gatekeeper.page.scss'],
})
export class GatekeeperPage implements OnInit {

  isLoading:boolean = false
  curMode:number
  modSub:Subscription
  isLoadingSub:Subscription
  constructor(private gkService:GatekeeperService) { }

  ngOnInit() {
    this.modSub = this.gkService.modeListener.subscribe(mNum => this.curMode = mNum)
    this.isLoadingSub = this.gkService.isLoadingListener.subscribe(is => {
      this.isLoading = is
    })
  }

  onSearchChange(event){
    const val = event.target.value;
    this.gkService.searchaData(val)

  }

}
