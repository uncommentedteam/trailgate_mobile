import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GatekeeperPage } from './gatekeeper.page';

describe('GatekeeperPage', () => {
  let component: GatekeeperPage;
  let fixture: ComponentFixture<GatekeeperPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatekeeperPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GatekeeperPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
