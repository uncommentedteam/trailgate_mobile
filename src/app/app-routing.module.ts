import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { AuthComponent } from './auth/auth/auth.component';

const routes: Routes = [
  // If dosent properlly the path redirect To auth
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
// Load a auth at the beggining
  {
    path: 'auth', component: AuthComponent
  },
  {
    path: 'gatekeeper',
    loadChildren: () => import('./gatekeeper/gatekeeper.module').then( m => m.GatekeeperPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'htc',
    loadChildren: () => import('./htc/htc.module').then( m => m.HtcPageModule),
    canLoad: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
