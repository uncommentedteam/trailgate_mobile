import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/top/user';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  loading:HTMLIonLoadingElement = null

  isAuthSub: Subscription;
  errMsgSub: Subscription;
  networkSub: Subscription;

  isLoading = false;
  errMsg:string = null
  constructor(private loadingCtrl:LoadingController,
              private authService:AuthService,
              private alertCtrl: AlertController) { 
  }

  ngOnInit() {
    this.errMsgSub = this.authService.errListener.subscribe(errMsg => this.errMsg = errMsg)

    this.isAuthSub = this.authService.isAuth.subscribe(isAuth =>{
      if(this.loading){
        this.loading.dismiss()
        this.loading = null
      }
      if (!isAuth && this.isLoading){
        if (this.errMsg !== null){
          this.showAlert(this.errMsg)
        }else {
          this.showAlert()
        }
      }
      this.isLoading = false
    })

    
    this.authService.GetStoredUser().then((storedUser:User) => {
      console.log("#########")
      console.log("stored user: ")
      if (storedUser){
        let contacto = storedUser.email
        const psw = storedUser.psw
        if (!contacto){
          contacto = storedUser.pnumber_1
        }
        this.showLoading().then(ionLoading => {
          this.loading = ionLoading
          this.loading.present()
          this.isLoading = true 
          this.authService.logIn(contacto, psw)
        })
        
      }
    }).catch( err => {
      console.log("############## Error: ", err)
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.isAuthSub.unsubscribe()
    this.errMsgSub.unsubscribe()

  }

  onEntrarclicked(form:NgForm){
    if (form.valid) {
      this.showLoading().then(ionLoading => {
        this.loading = ionLoading
        this.loading.present()
        this.isLoading = true 
        const contacto = form.value['contacto']
        const psw =  form.value['psw']
        this.authService.logIn(contacto, psw)
      })
    }
  }

  async showLoading(){
      return await this.loadingCtrl.create({
      spinner: "crescent",
      message: "Carregando . . .",
      duration: 60000,
      backdropDismiss: false,
    })
    // this.loading.present()
  }

  async showAlert(msg = "Nº de telemovel ou Email ou Palavra-passe invalidas!"){
    this.alertCtrl.create({
      message:msg,
      buttons: ['Ok']
    }).then( alert => alert.present())
  }

}
