import { GatekeeperService } from './../../services/gatekeeper.service';
import { Methods } from 'src/app/top/methods';
import { environment } from './../../../environments/environment';
import { ExtraVisit } from './../../top/extra_visit';
import { UserVisitor } from './../../top/user_visitor';
import { PedidoInfo } from './../../top/pedido_info';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { User } from 'src/app/top/user';
import { ModalMaterialComponent } from '../modal-material/modal-material.component';
import { HtcService } from 'src/app/services/htc.service';
import { Extra } from 'src/app/top/extra';

@Component({
  selector: 'app-modal-pedido',
  templateUrl: './modal-pedido.component.html',
  styleUrls: ['./modal-pedido.component.scss'],
})
export class ModalPedidoComponent implements OnInit {

  pedido: any;
  pedidoInfo: PedidoInfo[] = [];
  pedidoVisitor: UserVisitor[] = [];
  lstMaterial: ExtraVisit[] = [];
  lstMatricula: ExtraVisit[] = [];

  lstMPR: Extra[] = [];

  isLoadingInfo = false;
  isLoadingVisitor = false;
  isLoadingExtra = false;

  isVisitStarted = false;

  userAuthed: User;
  methods = new Methods();

  isNotify: Boolean;

  pedidoInfoSub: Subscription;
  pedidoVisitorSub: Subscription;
  pedidoExtraSub: Subscription;
  vCheckerSub: Subscription;
  extraLis: Subscription;
  gateSub: Subscription;
  isIos: boolean;
  lstGate: Extra[] = [];

  roleCanChange = { gate: [environment.core_cod] };

  constructor(private modalController: ModalController,
              private htcService: HtcService,
              private alertController: AlertController,
              private gkService: GatekeeperService,
              private plt: Platform) {
    this.isIos = this.plt.is('ios');
  }


  ionViewWillEnter(){
    this.userAuthed = this.htcService.curUser;
    this.isLoadingInfo = true;
    this.isLoadingVisitor = true;
    this.isLoadingExtra = true;

    this.isNotify = this.verifingNotificationTrigger();
    const cod_pedido = this.pedido.cod;
    const cod_mrp = environment.mrp_obs;
    this.htcService.serverGetPedidoInfo(cod_pedido);
    this.htcService.serverCheckVisitStarted(cod_pedido);
    this.htcService.serverGetExtras(cod_mrp);
    if (this.roleCanChange.gate) {
      this.gkService.serverGetGate();
    }
  }



  ngOnInit() {
    console.log(this.pedido);
    this.gateSub = this.gkService.gatesListener.subscribe(lGates => {
      this.lstGate = lGates;
    });

    this.pedidoInfoSub = this.htcService.pedidoInfoListener.subscribe(pInfo => {
      this.pedidoInfo = pInfo;
      this.isLoadingInfo = false;
    });

    this.pedidoVisitorSub = this.htcService.pedidoVisistoListener.subscribe(pVisit => {
      this.pedidoVisitor = pVisit;
      this.isLoadingVisitor = false;
    });

    this.pedidoExtraSub = this.htcService.pedidoExtraListener.subscribe(pExtra => {
      if (pExtra.length){
        [this.lstMaterial, this.lstMatricula] = this.methods.split_Extras(pExtra);
      }
      this.isLoadingExtra = false;
    });

    this.vCheckerSub = this.htcService.visitStartedListener.subscribe(started => {
      if (started || this.pedido.sv_is_done !== null) {
        this.isVisitStarted = true;
      } else {
        this.isVisitStarted = false;
      }
    });

    this.extraLis = this.htcService.lstExtraListener.subscribe(lstExtras => {
      this.lstMPR = lstExtras;
    });
  }

  verifingNotificationTrigger(){
    if (this.pedido['sv_is_done']) {
      return true
    }else if (this.userAuthed.cod_role === environment.core_cod) {
      return true
    }
    return false
  }

  ngOnDestroy() {
    this.pedidoInfoSub.unsubscribe();
    this.pedidoVisitorSub.unsubscribe();
    this.pedidoExtraSub.unsubscribe();
    this.vCheckerSub.unsubscribe();
    this.gateSub.unsubscribe();
  }


  dismiss(){
    this.modalController.dismiss();
  }

  notifyNext() {
    let idPedido = this.pedido['cod']
    const msg = "Colaborador foi notificado por favor aguarde ..."
    this.gkService.serverNotifyUser(idPedido).toPromise().then(_ => _);
    this.gkService.showToastAffterAction(msg)
  }

  setVisitTime(){
    return moment().format().split('T')[0] > moment(this.pedido.sv_visit_date).format().split('T')[0];
  }

  checkpendentInside(): Boolean{
    let ispending = false;
    if (this.pedido.length) {
      ispending = this.pedido[0].st_state === 0;
    } else {
      ispending = this.pedido.st_state === 0;
    }

    if (this.isVisitStarted && ispending){
      return true;
    }
    return false;
  }


  onMaterialClicked(){
    this.modalController.create({
      component: ModalMaterialComponent,
      componentProps: {lstMaterial: JSON.parse(JSON.stringify(this.lstMaterial))}
    }).then(
      modal => {
        modal.present();
        modal.onDidDismiss().then(value => {
          console.log(value.data);
          const lMtrl: ExtraVisit[] = value.data.data;
          const isDone: boolean = value.data.isDone;
          if (lMtrl.length > 0 && isDone){
            this.lstMaterial = lMtrl;
            this.htcService.serverChangeMaterial(this.pedido.cod, this.lstMaterial);
          }
        });
      }
    );
  }


  onPeriodoClick(){
    this.alertController.create({
      header: 'Escreva o numero de horas',
      message: 'As horas devem ser escritas em numeros interios.',
      inputs: [{
        name: 'horas',
        placeholder: 'Exp:. 12',
        type: 'number'
      }],
      buttons: ['OK', 'Cancel']
    }).then(alert => {
      alert.present();
      alert.onDidDismiss().then(dBack => {
        const horas: number = dBack.data.values.horas;
        if (horas > 0 && horas < 720){
          this.htcService.serverEditVisitSpec(this.pedido,
                                              true,
                                              horas);
          this.pedidoInfo[0].sv_period = horas;
        }
      });
    });
  }


  onGateClicked(){
    let gate: Number;
    const lstRb = [];
    for (let index = 0; index < this.lstGate.length; index++) {
      const extra: Extra = this.lstGate[index];
      const input = {
        name: extra.cod,
        type: 'radio',
        label: extra.extra,
        value: index,
        checked: false
      };
      lstRb.push(input);
    }
    lstRb[0].checked = true;
    this.alertController.create({
      header: 'Selecione o portão',
      inputs: lstRb,
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'danger',
        handler: () => {}
      }, {
        text: 'Ok',
        cssClass: 'success',
        handler: data => {
          const index = data;
          const extra: Extra = this.lstGate[index];
          gate = extra.cod;
          this.htcService.serverEditVisitSpec(this.pedido,
                                              false,
                                              null,
                                              true,
                                              gate);
        }
      }
    ]
    }).then(alert => {
      alert.present();
      alert.onDidDismiss();
    });



  }


  modalVisitor(visitor: UserVisitor){
    let prov: string;
    if (visitor.provenance != null){
      prov = visitor.provenance;
    } else if (visitor.entity != null){
      prov = visitor.entity;
    } else {
      prov = '-';
    }
    const nationality = visitor.nationality == null ? '-' : visitor.nationality;
    const email = visitor.email === null ? '-' : visitor.email;
    const pnumber = visitor.pnumber_1 === null ? '-' : visitor.pnumber_1;

    this.alertController.create({
      cssClass: 'alert-user-visitor',
      header: 'Dados do visitante:',
      subHeader: visitor.fname + ' ' + visitor.lname,
      message: '<hr><p><strong>Nº do ID: </strong>' + visitor.nid + '</p><hr>' +
              '<p><strong>Tipo de Id: </strong>' + visitor.type_id + '</p><hr>' +
              '<p><strong>Nacionalidade: </strong>' + nationality + '</p><hr>' +
              '<p><strong>Email: </strong>' + email + '</p><hr>' +
              '<p><strong>Tel: </strong>' + pnumber + '</p><hr>' +
              '<p><strong>Proviniencia: </strong>' + prov + '</p>',
      buttons: ['OK']
    }).then(alert => {
      alert.present();
    });
  }

  // ========= Accepting
  onYesClick(){
    // USER INFO
    const uRole = this.userAuthed.cod_role;
    // let uTerminal = this.userAuthed.cod_company
    const uCode = this.userAuthed.cod;

    // REQUEST DETAIL

    const request_cod_host = this.pedidoInfo[0].st_host_cod;
    // let request_cod_terminal = this.pedidoInfo[0]['sv_cod_comp']
    const is_2_company = this.pedidoInfo[0].sv_is_company;
    // let host_cod_role =  this.pedidoInfo[0]['st_host_cod_role']
    // let thread_cod_role = this.pedidoInfo[0]['st_thread_cod_role']
    // let core_cod_role = this.pedidoInfo[0]['st_core_cod_role']

    // CURRENT STATE
    const state_host_cod = this.pedido.st_host_cod;
    const state_thread_cod = this.pedido.st_threa_cod;
    // let state_core_cod = this.pedido['st_core_id']
    const state_host = this.pedido.st_host_is;
    const state_thread = this.pedido.st_threa_is;
    const state_core = this.pedido.st_core_is;
    const state_host_cancel = this.pedido.st_host_is_cancel;
    const state_thread_cancel = this.pedido.st_threa_is_cancel;
    const state_core_cancel = this.pedido.st_core_is_cancel;


    // Check if he is the host
    if (uRole === environment.host_cod) {
      this.hostAccepting(uCode, state_host_cod, state_host, state_host_cancel);

    } else if (uRole === environment.thread_cod){
      // Se eu sou o thread dessa terminal
      this.threadAccepting(uCode, request_cod_host, state_thread_cod, state_thread, state_thread_cancel, is_2_company, state_host);

    } else {
      // Se eu sou o core
      this.coreAcceptimg(state_core, state_core_cancel, request_cod_host, uCode, state_thread);

    }
  }

  hostAccepting(uCode, state_host_cod, state_host, state_host_cancel){
      if (state_host && !state_host_cancel){
        // MSG VOCE JA ACEITOU ESTE PEDIDO
        this.showAlert('Pedido Aceite', 'O Pedido ja foi aceitou !');
      } else {
        // VERIFICAR SE ESTA CANCELADO
        if (state_host_cancel){
          // Perguntar se quer descancelar
          if (state_host_cod === uCode){
            this.showAlertConfirmation('Deseja Descancelar o pedido?', 0, null, true);
          } else {
            this.showAlert('Pedido Cancelado', 'Pedido foi cancelado por um outro ' + this.userAuthed.user_role);
          }
        } else {
          // Aceitar
          this.showAlertConfirmation('Deseja Aceitar o pedido?', 1, null, false);
        }
      }
  }

  threadAccepting(uCode, request_cod_host, state_thread_cod, state_thread, state_thread_cancel, is_2_company, state_host){
      if (state_thread && !state_thread_cancel){
        // MSG VOCE JA ACEITOU ESTE PEDIDO
        this.showAlert('Pedido Aceite', 'O Pedido ja foi aceitou !');
      } else {
        // VERIFICAR SE ESTA CANCELADO
        if (state_thread_cancel){
          // Verificar se quem cancelou foi o thread
          if (state_thread_cancel && state_thread_cod === uCode){
            // POSSO DESCANCELAR
            this.showAlertConfirmation('Deseja Descancelar o pedido?', 0, null, true);
          } else {
            this.showAlert('Pedido Cancelado', 'Pedido foi cancelado por um outro ' + this.userAuthed.user_role);
          }
        } else {
          // Aceitar
          if (request_cod_host === null){
            this.htcService.serverAddHost(this.pedido.cod);
          }
          if (request_cod_host === null ||  request_cod_host === uCode || state_host){
            this.showAlertConfirmation('Deseja Aceitar o pedido?', 1, null, false);
          } else {
            this.showAlert('Pedido Pendente', 'O pedido ainda não foi aceite pelo anfitriao');
          }

        }
      }
  }

  coreAcceptimg(state_core, state_core_cancel, request_cod_host, uCode, state_thread){
      if (state_core && !state_core_cancel){
        // MSG VOCE JA ACEITOU ESTE PEDIDO
        this.showAlert('Pedido Aceite', 'O Pedido ja foi aceitou !');
      } else {
        // VERIFICAR SE ESTA CANCELADO
        if (state_core_cancel){
          // Perguntar se quer descancelar
          this.showAlertConfirmation('Deseja Descancelar o pedido?', 0, null, true);
        } else {
          // Aceitar
          if (request_cod_host === uCode || state_thread){
            this.showAlertConfirmation('Deseja Aceitar o pedido?', 1, null, false);
          } else {
            this.showAlert('Pedido Pendente', 'O pedido ainda não foi aceite pelos outros colaboradores');
          }
        }
      }
  }
  // ========= End Accepting


  // ========= Start Refusing
  onNoClick(){
    // USER INFO
    const uRole = this.userAuthed.cod_role;
    // let uTerminal = userAuthed.cod_company
    const uCode = this.userAuthed.cod;

    // REQUEST DETAIL

    const request_cod_host = this.pedidoInfo[0].st_host_cod;
    // let request_cod_terminal = this.pedidoInfo[0]['sv_cod_comp']
    const is_2_company = this.pedidoInfo[0].sv_is_company;

    // CURRENT STATE
    const state_host_cod = this.pedido.st_host_cod;
    const state_thread_cod = this.pedido.st_threa_cod;
    const state_core_cod = this.pedido.st_core_id;
    const state_host = this.pedido.st_host_is;
    const state_thread = this.pedido.st_threa_is;
    const state_core = this.pedido.st_core_is;
    const state_host_cancel = this.pedido.st_host_is_cancel;
    const state_thread_cancel = this.pedido.st_threa_is_cancel;
    const state_core_cancel = this.pedido.st_core_is_cancel;

    if (uRole === environment.host_cod){
      this.hostNoAction(uCode, state_host_cod, state_host, state_host_cancel);

    } else if (uRole === environment.thread_cod){
      this.ThreadNoAction(uCode, request_cod_host, state_thread_cod, state_thread, state_thread_cancel, is_2_company);

    } else {
      this.CoreNoAction(uCode, state_thread,  state_thread_cancel, state_core_cod, state_core, state_core_cancel);
    }
  }

  hostNoAction(uCode, state_host_cod, state_host, state_host_cancel){
    if (state_host){
      if (state_host_cancel){
        // MSG VISITA JA ESTA CANCELADA
        this.showAlert('Pedido Cancelado', 'O pedido ja foi cancelado!');
      }else{
        // Cancelar
        if (state_host_cod === uCode || state_host_cod === null){
          this.showAlertRefuse('Porque deseja cancelar o pedido ?', 1);
        }else{
          this.showAlert('Cancelar Pedido', 'Não pode cancelar um pedido aceite por um outro usuario');
        }
      }
    } else {
      // Recusar
      if (state_host === false){
        this.showAlert('Pedido Recusado', 'O pedido ja foi recusado!');
      } else {
        this.showAlertRefuse('Porque deseja recusar o pedido ?', 0);
      }
    }
  }

  ThreadNoAction(uCode, request_cod_host, state_thread_cod, state_thread, state_thread_cancel, is_2_company){
    if (state_thread){
      if (state_thread_cancel){
        // MSG VISITA JA ESTA CANCELADA
        this.showAlert('Pedido Cancelado', 'O pedido ja foi cancelado!');
      }else{
        // Cancelar
        if (state_thread_cod === uCode || state_thread_cod === null){
          if (state_thread){
            this.showAlertRefuse('Porque deseja cancelar o pedido ?', 1);
          } else {
            this.showAlertRefuse('Porque deseja recusar o pedido ?', 0);
          }
        }else{
          if (state_thread){
            this.showAlert('Cancelar Pedido', 'Não pode cancelar um pedido aceite por um outro usuario');
          }else{
            this.showAlert('Recusar Pedido', 'Não pode recusar um pedido aceite por um outro usuario');
          }
        }
      }
    } else {
      // Recusar
      if (request_cod_host === null && is_2_company){
        this.htcService.serverAddHost(this.pedido.cod);
      }
      this.showAlertRefuse('Porque deseja recusar o pedido ?', 0);
    }
  }

  CoreNoAction(uCode, state_thread, state_thread_cancel, state_core_cod, state_core, state_core_cancel){
    if (state_thread){
      if (state_core_cancel){
        // MSG VISITA JA ESTA CANCELADA
        this.showAlert('Pedido Cancelado', 'O pedido ja foi cancelado');
      }else{
        // Cancelar
        if (state_core_cod === uCode || state_core_cod === null){
          if (state_core){
            this.showAlertRefuse('Porque deseja cancelar o pedido ?', 1);
          } else {
            this.showAlertRefuse('Porque deseja recusar o pedido ?', 0);
          }
        }else{
          if (state_core){
            this.showAlert('Cancelar Pedido', 'Não pode cancelar um pedido aceite por um outro usuario');
          }else{
            this.showAlert('Recusar Pedido', 'Não pode recusar um pedido aceite por um outro usuario');
          }
        }
      }
    } else {
      // Recusar
      this.showAlertRefuse('Porque deseja recusar o pedido ?', 0);
    }
  }
  // ========= End Refusing


  // ALERTS
  async showAlert(header, msg){
    this.alertController.create({
      header,
      message: msg,
      buttons: ['Ok']
    }).then( alert => alert.present());
  }


  showAlertConfirmation(txt, state, txt_why = null, isCancel){
    this.alertController.create({
      header: txt,
      buttons: [{
        text: 'NO',
        role: 'cancel',
        cssClass: 'danger',
      }, {
        text: 'YES',
        cssClass: 'success',
        handler: () => {
          if (isCancel){
            this.htcService.serverCancelPedidoState(this.pedido.cod, state, txt_why);
          }else{
            this.htcService.serverChangePedidoState(this.pedido.cod, state, txt_why);
          }
          this.modalController.dismiss();
          this.alertController.dismiss();
        }
      }]
    }).then(alert => alert.present());
  }


  showAlertRefuse(header, isCancel){
    const lstRb = [];
    let createdAlert: HTMLIonAlertElement;

    for (let index = 0; index < this.lstMPR.length; index++) {
      const extra: Extra = this.lstMPR[index];
      const input = {
        name: extra.cod,
        type: 'radio',
        label: extra.extra,
        value: index,
        checked: false
      };
      lstRb.push(input);
    }

    this.alertController.create({
      header,
      backdropDismiss: true,
      cssClass: 'alert-mpr',
      inputs: lstRb,
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'danger',
        handler: () => {}
      }, {
        text: 'Ok',
        cssClass: 'success',
        handler: index => {
          const extra: Extra = this.lstMPR[index];
          createdAlert.dismiss();
          if (isCancel){
            this.htcService.serverCancelPedidoState(this.pedido.cod, 1, extra.cod);
          }else{
            this.htcService.serverChangePedidoState(this.pedido.cod, 0, extra.cod);
          }
        }
      }
    ]
    }).then(alert => {
      createdAlert = alert;
      alert.present();
      alert.onDidDismiss();
    });
  }

  // END
}

