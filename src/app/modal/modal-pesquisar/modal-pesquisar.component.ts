import { User } from './../../top/user';
import { Component, OnInit } from '@angular/core';
import { GatekeeperService } from 'src/app/services/gatekeeper.service';
import { Subscription } from 'rxjs';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-pesquisar',
  templateUrl: './modal-pesquisar.component.html',
  styleUrls: ['./modal-pesquisar.component.scss']
})
export class ModalPesquisarComponent implements OnInit {

  lstUser:User[] = []
  lstUserBK:User[] = []
  isLoading = false 
  userLis:Subscription
  constructor(private gkService:GatekeeperService, private modalController: ModalController) { }

  ionViewWillEnter(){
    
    if (this.gkService.listAllUsers.length > 0){
      this.isLoading = false 
      this.lstUser = this.gkService.listAllUsers
      if (this.lstUser.length >0){
        this.lstUserBK = this.gkService.listAllUsers
      }
    } else {
      this.isLoading = true
      this.gkService.serverGetAllVisitor()
    }
  }

  ngOnInit(): void {
    this.userLis = this.gkService.visitorsLis.subscribe(lUser => {
      this.isLoading = false 
      this.lstUser = lUser
      if (lUser.length >0){
        this.lstUserBK = lUser
      }
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if (this.userLis){
      this.userLis.unsubscribe()
    }
  }

  itemHeightFn(item, index) {
    return 100
  }

  trackByFn(index:number, item:any){
    if (item.length){ 
      return item[0].cod
    } else 
      return item.cod
  }

  onItemClick(user){
    console.log(user)
    this.modalController.dismiss(user)
  }

  search(val){
    if (val === '' || val === ' '){
      this.lstUser = this.lstUserBK
    } else {
      this.lstUser = this.lstUser.filter((item:User) => {
        var fname = item.fname.toLocaleLowerCase()
        var lname = item.lname.toLocaleLowerCase()
        var fullName = fname+' '+lname
        var n
        if (item.nid !== null) {
          n = item.nid
        } else {
          n = item.pnumber_1
        }
        if (fname.indexOf(val.toLocaleLowerCase()) > -1||
        lname.indexOf(val.toLocaleLowerCase()) > -1||
        fullName.indexOf(val.toLocaleLowerCase()) > -1||
        n.indexOf(val.toLocaleLowerCase()) > -1){
        return item  
        }
      })
    }
  }

  dismiss(){
    this.modalController.dismiss(null)
  }
}
