import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPesquisarComponent } from './modal-pesquisar.component';

describe('ModalPesquisarComponent', () => {
  let component: ModalPesquisarComponent;
  let fixture: ComponentFixture<ModalPesquisarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPesquisarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPesquisarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
