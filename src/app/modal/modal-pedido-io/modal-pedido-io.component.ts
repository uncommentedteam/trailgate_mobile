import { ExtraVisit } from './../../top/extra_visit';
import { Methods } from 'src/app/top/methods';
import { PedidoIO } from 'src/app/top/PedidoIO';
import { Component, OnInit } from '@angular/core';
import { AlertController, IonItemSliding, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Extra } from 'src/app/top/extra';
import { ModalMaterialComponent } from '../modal-material/modal-material.component';
import * as moment from 'moment';
import { GatekeeperService } from 'src/app/services/gatekeeper.service';

@Component({
  selector: 'app-modal-pedido-io',
  templateUrl: './modal-pedido-io.component.html',
  styleUrls: ['./modal-pedido-io.component.scss'],
})
export class ModalPedidoIoComponent implements OnInit {

  visitorObj:PedidoIO =null
  lstObjVisitor:any

  isEntrance:boolean
  isStatus_rule:boolean = true
  isEntrance_rule:boolean = true
  isPeriodo_rule:boolean = true
  isInduction_rule:boolean = true
  isGettingMaterial:boolean = false
  isNotifying:boolean = false

  lstMaterial:ExtraVisit[] = []
  lstMatricula:ExtraVisit[] = []
  lstMPR:Extra[] = []

  methods = new Methods()

  extrasListener:Subscription
  mprListener:Subscription
  gettingDataErrorListener:Subscription 
  acceptedListener:Subscription

  constructor(private modalController: ModalController, 
              private gkService:GatekeeperService,
              private alertController: AlertController) { }


  ionViewWillEnter(){
    if (this.lstObjVisitor.length){
      this.visitorObj = this.lstObjVisitor[0]
    } else {
      this.visitorObj = this.lstObjVisitor
    }
    
    let cod_pedido = this.visitorObj.cod

    this.isGettingMaterial = true
    this.gkService.serverGetPedidoExtra(cod_pedido)
    this.gkService.serverGetMPR(this.isEntrance)
    this.checkStatus()
    this.expiredPeriod()
    this.checkEntraceGeral()
    this.checkInductionGeral()
  }


  ngOnInit() {
    this.extrasListener = this.gkService.extrasListener.subscribe(lExtras => {
      [this.lstMaterial, this.lstMatricula] = this.methods.split_Extras(lExtras)
      this.isGettingMaterial = false
    })

    this.mprListener = this.gkService.mprListener.subscribe(lMPR => {
      this.lstMPR = lMPR
    })

    this.gettingDataErrorListener = this.gkService.getDataError.subscribe( hasErr => {
      if (hasErr){
        this.showAlert("Error obtendo os dados. Tente novamente!")
      }
    }
    )

    this.acceptedListener = this.gkService.posAprovedListener.subscribe(pedidoAproved => {
      if (pedidoAproved !== null){
        if (this.lstObjVisitor.length > 1) {
          for (let index = 0; index < this.lstObjVisitor.length; index++) {
            const pedido = this.lstObjVisitor[index];
            if (pedido.cod === pedidoAproved.cod &&  
              pedido.x_cod === pedidoAproved.x_cod){
              this.lstObjVisitor.splice(index, 1)
              break
            }
          }
        } else {
          this.dismiss()
        }
      }  
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.gettingDataErrorListener.unsubscribe()
    this.mprListener.unsubscribe()
    this.extrasListener.unsubscribe()
    this.acceptedListener.unsubscribe()
   
  }


  checkInduction(visitor){
    if (visitor['to_induct']) {
      //APROVED
      return true
    }
    else if (visitor['to_induct'] === false ){
      //REPROVADO
      return false 
    } else {
      //To Induct 
      return false 
    }
  }

  checkInductionGeral(){
    if (this.lstObjVisitor.length) {
      for (let index = 0; index < this.lstObjVisitor.length; index++) {
        const visitor = this.lstObjVisitor[index];
        if (!this.checkInduction(visitor)) {
          this.isInduction_rule = false
          break
        }
      }
    }else{
      this.isInduction_rule = this.checkInduction(this.visitorObj)
    }
  }

  
  checkStatus(){
    this.isStatus_rule = this.visitorObj['st_state'] === 1 
  }


  checkEntrace(visitor){
    if (visitor['sv_is_multi_entry']){
      return true
    } else {
      // first time in 
      if (visitor['visit_n_in'] > 0){
        return false 
      } else 
        return true
    }
  }

  checkEntraceGeral(){
    if (this.lstObjVisitor.length) {
      for (let index = 0; index < this.lstObjVisitor.length; index++) {
        const visitor = this.lstObjVisitor[index];
        if (!this.checkEntrace(visitor)) {
          this.isEntrance_rule = false
          break
        }
      }
    }else{
      this.isEntrance_rule = this.checkEntrace(this.visitorObj)
    }
  }


  expiredPeriod(){
    var lastTimeIn
    var periodo 
    if (this.lstObjVisitor.length){
      this.lstObjVisitor.sort(function(a, b) {
        // convert date object into number to resolve issue in typescript
        return  +new Date(a.sv_visit_date) - +new Date(b.sv_visit_date);
      })
      lastTimeIn = this.lstObjVisitor[0]['las_dtm_in']
      periodo = this.lstObjVisitor[0]['sv_period']
    } else {
      lastTimeIn = this.visitorObj['las_dtm_in']
      periodo = this.visitorObj['sv_period']
    }
    this.isPeriodo_rule = moment().format() <= moment(lastTimeIn).add(periodo, 'hours').format()
  }


  onMaterialClicked(){
    this.modalController.create({
      component:ModalMaterialComponent,
      componentProps:{'lstMaterial':JSON.parse(JSON.stringify(this.lstMaterial))}
    }).then(
      modal => {
        modal.present()
        modal.onDidDismiss().then(value =>{
          console.log(value.data)
          const lMtrl:ExtraVisit[] = value.data['data']
          const isDone:boolean = value.data['isDone']
          if (lMtrl.length > 0 && isDone){
            this.lstMaterial = lMtrl
            this.gkService.serverChangeMaterial(this.visitorObj.cod, this.lstMaterial)
          }
        })
      }
    )
  }


  onBtnClicked(is:boolean){
    console.log(is)
    if (!is){
      this.showAlertMPR()
    } else {
      this.gkService.serverGatekeeperIo(this.lstObjVisitor,1,this.isEntrance)
      /*if (!this.isEntrance){
        this.setIsDone()
      }*/
    }
  }


  onActionSlideClicked(sItem:IonItemSliding, i:number, is:boolean){
    sItem.closeOpened()
    const visitor:PedidoIO = this.lstObjVisitor[i]
    console.log(visitor)
    if (is){
      this.gkService.serverGatekeeperIo(visitor,1,this.isEntrance)
    }else {
      this.showAlertMPR(visitor)
    } 
  }


  /*setIsDone(){
    const vCode = this.visitorObj.cod
    if (!this.isEntrance) {
      if (this.visitorObj.sv_is_multi_entry){
        if (!this.isPeriodo_rule){
          // Set is done true
          this.gkService.serverSetVisitDone(vCode)
        }
        console.log("Visita #"+vCode+" ainda nao terminou ")
      } else {
        this.gkService.serverSetVisitDone(vCode)
      }
    }
  }*/


  dismiss(){
    this.modalController.dismiss()
  }


  notify() {
    
    this.isNotifying = true
    this.gkService.serverNotifyUser(this.visitorObj.cod).subscribe(response =>  {
      console.log("The host has been notified")
    }, error => {
      console.log("error: ")
      console.log(error)
    })
    const msg = "Colaborador foi notificado por favor aguarde ..."
    this.gkService.showToastAffterAction(msg)
  }


  showAlertMPR(visitor?:PedidoIO){
    let lstRb = []
    for (let index = 0; index < this.lstMPR.length; index++) {
      const extra:Extra = this.lstMPR[index]
      const input = {
        name: extra.cod,
        type:'radio',
        label:extra.extra,
        value:index,
        checked:false
      } 
      lstRb.push(input)
    }
    let createdAlert:HTMLIonAlertElement
    this.alertController.create({
      header: 'Selecione a opção',
      backdropDismiss: true,
      cssClass:'alert-mpr',
      inputs: lstRb,
      buttons: [{
        text:'Cancelar',
        role:'cancel',
        cssClass:'danger',
        handler: () => {}
      },{
        text:'Ok',
        cssClass:'success',
        handler: index => {
          const extra:Extra = this.lstMPR[index]
          createdAlert.dismiss()
          if (visitor){
            this.gkService.serverGatekeeperIo(visitor,0,this.isEntrance, extra.cod)
          } else {
            this.gkService.serverGatekeeperIo(this.lstObjVisitor,0,this.isEntrance,extra.cod)
          }
        }
      }
    ]
    }).then(alert => {
      createdAlert = alert
      alert.present()
      alert.onDidDismiss()
    })
  }

  async showAlert(msg:string) {
    this.alertController.create({
      message: msg,
      buttons: ['Ok']
    }).then( alert => alert.present())
  }

}
