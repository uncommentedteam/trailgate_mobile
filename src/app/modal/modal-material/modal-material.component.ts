import { Methods } from './../../top/methods';
import { ExtraVisit } from './../../top/extra_visit';
import { Component, OnInit } from '@angular/core';
import {ModalController, Platform, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-modal-material',
  templateUrl: './modal-material.component.html',
  styleUrls: ['./modal-material.component.scss'],
})
export class ModalMaterialComponent implements OnInit {

  constructor(private modalController: ModalController,
              private toast: ToastController,
              private plt:Platform) { 
    this.isIos = this.plt.is("ios")
  }

  methods = new Methods

  lstMaterial:ExtraVisit[] = []
  qnt = 1
  material = ''

  isDone:boolean = false
  isIos:boolean

  ngOnInit() {}

  onRemoveClick(index: number, clickedMtrl:ExtraVisit){
    if (clickedMtrl.int_id) {
      this.lstMaterial[index].bool_is_active = false;
    } else {
      this.lstMaterial = this.lstMaterial.filter((_, i) => i !== index);
    }
  }


  onAddClick(){

    if (this.material !== '' && this.qnt > 0){
      const nMaterial:ExtraVisit = {
        int_id: null,
        bool_is_active: true,
        bool_ismtrl: true,
        int_qnt:this.qnt,
        str_oname:this.material
      }

      const hasThatMtrl = this.lstMaterial.filter(mtrl => mtrl.str_oname.toLowerCase() === nMaterial.str_oname.toLowerCase() && mtrl.bool_is_active).length
      if (hasThatMtrl < 1) {
        this.lstMaterial.push(nMaterial)
        this.qnt = 1
        this.material = ''
      }
    } 
    
  }


  dismiss(){
    this.modalController.dismiss({'data':[],'isDone':this.isDone})
  }

  done(){
    this.isDone = true
    this.modalController.dismiss({'data':this.lstMaterial,'isDone':this.isDone})
  }
}
