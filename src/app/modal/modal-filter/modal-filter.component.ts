
import { environment } from './../../../environments/environment';
import { ModalController, Platform } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { User } from 'src/app/top/user';
import { Subscription } from 'rxjs';
import { Terminal } from 'src/app/top/terminal';
import { Department_Filter } from 'src/app/top/department_filter';
import { HtcService } from 'src/app/services/htc.service';

@Component({
  selector: 'app-modal-filter',
  templateUrl: './modal-filter.component.html',
  styleUrls: ['./modal-filter.component.scss'],
})
export class ModalFilterComponent implements OnInit {

  dInicio:string = moment().format().split('T')[0]
  dFim:string = moment().add(30, 'days').format().split('T')[0]
 
  curTipVisi:number = 0
  curTipPedido:number = 0
  curTipEstado:number = -2

  @Input() user:User

  curTerminal:number = null
  curDepartamento:number = null
  curUser:number = null

  lstObjTer:Terminal[]
  lstObjDep:Department_Filter[]
  lstObjHost:User[]

  env = environment
  terminalListener: Subscription
  depListener: Subscription
  userListener: Subscription
  
  terminalIsLoading = false
  depdIsLoading = false
  hostIsLoading = false

  isCore = false
  isThread= false
  isHost = false 

  maxYear = moment
  // List To help on build
  lstEstadoPedido = [
    {'id':-2, 'name':'Todos'},
    {'id':1, 'name':'Aceites'},
    {'id':-1, 'name':'Recusados'},
    {'id':0, 'name':'Pendentes'},
    {'id':2, 'name':'Cancelados'},
  ]
  
  lstTipVisita = [
      {'id':0, 'name':'Todas'},
      {'id':1, 'name':'Singulares'},
      {'id':2, 'name':'Colectivas'},
    ]

  listTipPedido = [
      {'id':0, 'name':'Todos'},
      {'id':1, 'name':'Acesso'},
      {'id':2, 'name':'Saida'},
    ]
  
  isIos:boolean

  constructor(private modalCtrl:ModalController,
              private htcService:HtcService,
              private plt:Platform) { 
    this.isIos = this.plt.is("ios")
  }

  ngOnInit() {

    console.log(this.user)
    // Core
    if (this.user.cod_role === this.env.core_cod){
      this.isCore = true
      this.terminalIsLoading = true
      this.htcService.serverGetListTerminal() 
    }
    // Thread
    else if (this.user.cod_role === this.env.thread_cod){
      this.isThread = true
      this.depdIsLoading = true
      this.hostIsLoading = true 
      this.curTerminal = this.user.cod_company
      const defTerm = this.user.cod_company
      const defDep = this.user.cod_department
      this.htcService.serverGetLisDep(defTerm)
      this.htcService.serverGetListUser(defTerm, defDep)
    }
    // Host
    else{
      this.isHost = true
      this.curTerminal = this.user.cod_company
      this.curDepartamento = this.user.cod_department
      this.curUser = this.user.cod
    }

    // Listeners
    this.terminalListener = this.htcService.terminalListener.subscribe(lTerm =>{
      this.lstObjTer = lTerm
      this.terminalIsLoading = false
    })

    this.depListener = this.htcService.depListener.subscribe(lDep => {
      this.lstObjDep = lDep
      this.depdIsLoading = false 
    })

    this.userListener = this.htcService.hostListener.subscribe(lHost => {
      this.lstObjHost = lHost
      this.hostIsLoading = false
    })

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.terminalListener.unsubscribe()
    // this.depListener.unsubscribe()
    // this.userListener.unsubscribe()
  }

  dismiss(){
    this.modalCtrl.dismiss({'data':null, 'isDone':false})
  }

  // EventHandlers
  onTipoVisitChange(kVal){
    this.curTipVisi = kVal
  }

  onEstadoVisitChange(kVal){
    this.curTipEstado = kVal
  }

  onTipoPedidoChange(kVal){
    this.curTipPedido = kVal
  }

  onTerminalChange(event){
    this.curTerminal = event.detail.value
    this.depdIsLoading = true
    this.htcService.serverGetLisDep(this.curTerminal)
  }

  onDepChange(event){
    this.curDepartamento = event.detail.value
    this.hostIsLoading = true
    this.htcService.serverGetListUser(this.curTerminal, this.curDepartamento)
  }

  onHostChange(even){
    this.curUser = even.detail.value
  }

  onFiltarClick(){
    this.modalCtrl.dismiss({'data':{
      'cod_ter':this.curTerminal,
      'cod_dep':this.curDepartamento,
      'cod_host':this.curUser,
      'dtm_inicio':this.dInicio,
      'dtm_fim':this.dFim,
      'tip_visit':this.curTipVisi,
      'tip_pedido':this.curTipPedido,
      'tip_estado':this.curTipEstado,
    }, 'done':true})

  }


}

