import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HtcPageRoutingModule } from './htc-routing.module';

import { HtcPage } from './htc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HtcPageRoutingModule
  ],
  declarations: [HtcPage]
})
export class HtcPageModule {}
