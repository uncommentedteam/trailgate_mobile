import { NgForm } from '@angular/forms';
import { Extra } from './../../top/extra';
import { environment } from 'src/environments/environment';
import { Paises } from './../../top/paises';
import { Subscription } from 'rxjs';
import { User } from 'src/app/top/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HtcService } from 'src/app/services/htc.service';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.scss'],
})
export class DadosComponent implements OnInit {

  isAuthListener:Subscription
  lstTipIdListener:Subscription
  isLoadingTid:boolean = false
  isAuthed:boolean = false
  user:User = null
  paises= new Paises()
  COUNTRY_LIST = this.paises.COUNTRY_LIST
  selected_country = ""
  selected_TipId:number
  selected_expDate:string
  psw = ''

  lstTipId:Extra[]

  constructor(private htcService: HtcService, 
              private authService:AuthService) { }

  ionViewWillEnter() {
    this.htcService.changeMode(2)
    const codTipId = environment.tid_cod
    this.isLoadingTid = true
    this.htcService.serverGetExtras(codTipId)
  }
  
  ngOnInit() {
    this.user = this.authService.getUser()
    this.selected_TipId = this.user.cod_type_id
    this.selected_expDate = this.user.exp_date_id
    this.selected_country = this.user.nationality

    this.isAuthListener = this.authService.isAuth.subscribe(isAuth => {
      this.isAuthed = isAuth
      this.selected_TipId = this.user.cod_type_id
      this.selected_expDate = this.user.exp_date_id
      this.selected_country = this.user.nationality
    })

    this.lstTipIdListener = this.htcService.lstExtraListener.subscribe(lEx => {
      this.lstTipId = lEx

      this.isLoadingTid = false
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.isAuthListener.unsubscribe()
    this.lstTipIdListener.unsubscribe()
  }

  onSubmit(form: NgForm){
    console.log(this.user)
    let activo 
    if (this.user.isactive){
      activo = 1
    } else {
      activo = 0
    }

    let bParm = {
      'p_id': this.user.cod,
      'p_fname':this.user.fname,
      'p_lname':this.user.lname,
      'p_nid':this.user.nid,
      'p_email':this.user.email,
      'p_pnumber_1':this.user.pnumber_1,
      'p_pnumber_2':this.user.pnumber_2,
      'p_psw':this.psw,
      'p_active':activo,
      'p_isvisitor':0,
      'p_nationality':this.user.nationality,
      'p_id_type_id':this.user.cod_type_id,
      'p_expiration_date_id': this.user.exp_date_id
    }
    this.authService.serverUpdateUserInfo(bParm, this.user)
    // this.loadingCtrl.create({
    //   message: 'Atualizando os dados por favor aguarde . . .',
    //   translucent: true,
    // }).then(loading => {
    //   loading.present()
    //   this.authService.updateUserData(loading, bParm)
    // })
    console.log(bParm)
  }

  onRbTipIdChange(event:CustomEvent){
    this.user.cod_type_id = event.detail.value
  }


  onRbNacionalidadeChange(event:CustomEvent){
    this.user.nationality = event.detail.value

  }


  onDataChange(event:CustomEvent){
    this.user.exp_date_id = event.detail.value.split('T')[0]
  }
}
