import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ModalPedidoComponent } from 'src/app/modal/modal-pedido/modal-pedido.component';
import { HtcService } from 'src/app/services/htc.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  isInitLoad:boolean = false
  isLoading:boolean = false
  isRefreshing:boolean = false

  refresherEvent = null
  lstDictVisits:any = null

  triggerRefreshSubs:Subscription
  dashboardSubs:Subscription

  constructor(private htcService:HtcService, private modalController: ModalController) { }

  ionViewWillEnter() {
    this.isInitLoad = true
    this.isLoading = true
    this.htcService.changeMode(0)
    this.htcService.serverGetDashboardData()
  }


  ngOnInit() {
    // this.htcService.socket.on('update', () => {
    //   this.isLoading = true
    //   this.htcService.serverGetDashboardData()
    // })
    this.triggerRefreshSubs = this.htcService.triggerRefreshListener.subscribe(triggered => {
      if (triggered){
        this.isLoading = true
        this.htcService.serverGetDashboardData()
      }else{
        console.log("Something Happen")
      }
    })

    this.dashboardSubs = this.htcService.dashDataListener.subscribe(dData => {
      console.log(dData)
      this.isInitLoad = false
        this.isLoading = false
        if (this.refresherEvent){
          this.refresherEvent.target.complete()
          this.refresherEvent = null
        }
        
      if (dData !== null){
        this.lstDictVisits = Object.keys(dData).map(function(key){
          return dData[key];
        });
      }
    })  
  }

  onRefresh(event) {
    this.isLoading = true
    this.refresherEvent = event
    this.htcService.serverGetDashboardData()
  }

  private _checkVisit_G(lstVisitObj): Number {
    var status: Number = 1
    var last_status: Number

    for (let visit of lstVisitObj){
      // Se teve algum problema na entrada ou saida
      if (!this.onCheckStatus(visit) && visit['st_state'] === 1) {
        return -1
      } 
      // Se estiver la fora 
      if (this.onCheckStatus(visit) && !visit['bool_isin'] && !visit['sv_is_done'] && 
      (visit['st_state'] !== 2 && visit['st_state'] !== -1)){
        last_status = 0
      }

      if (this.onCheckStatus(visit) && visit['sv_is_done']){
        last_status = 2
      }

      if (last_status === 2 && status === 1 ){
        status = 2
      } else if (last_status === 0) {
        status = 0
      }
    }
    return status
  }


  checkVisit(visitOjs){
    // -1 RED
    // 0 ORANGE
    // 1 GREEN
    // 2 GRAY

    if (visitOjs.length) {
      // Colectivo
     return this._checkVisit_G(visitOjs)

    } else {
      // Singular
      let visit = visitOjs
      // Se teve algum problema na entrada ou saida
      if (!this.onCheckStatus(visit) && visit['st_state'] === 1) {
        return -1
      } 
      // Se estiver la fora 
      if (this.onCheckStatus(visit) && !visit['bool_isin'] && !visit['sv_is_done'] && 
      (visit['st_state'] !== 2 && visit['st_state'] !== -1)){
        return 0
      }

      if (this.onCheckStatus(visit) && visit['sv_is_done']){
        return 2
      }
      return 1

    }
  }


  checkEach(visit){
    let isObs = this.onCheckStatus
    let isDone = visit['sv_is_done']
    let state = visit['state']
  }

  onCheckStatus(visit){
    return visit['int_id_obs'] === null
  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.dashboardSubs.unsubscribe()
    this.triggerRefreshSubs.unsubscribe()
  }
  

  onClick(visit){
    if (visit.length){
      visit = visit[0]
    }
    this.modalController.create({
      component:ModalPedidoComponent,
      componentProps:{'pedido':visit}
    }).then(modal => {
      modal.present()
    })

  }

}
