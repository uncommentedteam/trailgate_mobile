import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DadosComponent } from './dados/dados.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { HtcPage } from './htc.page';
import { PedidoComponent } from './pedido/pedido.component';

const routes: Routes = [
  {
    path: '',
    component: HtcPage,
    children: [
      {
        path: 'dashboard', component: DashboardComponent
      },
      {
        path: 'pedido', component: PedidoComponent
      },
      {
        path: 'dados', component: DadosComponent
      },
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HtcPageRoutingModule {}
