import { Subscription } from 'rxjs';
import { Pedido } from './../../top/pedido';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ModalController } from '@ionic/angular';
import { ModalPedidoComponent } from 'src/app/modal/modal-pedido/modal-pedido.component';
import { Methods } from 'src/app/top/methods';
import { HtcService } from 'src/app/services/htc.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss'],
})
export class PedidoComponent implements OnInit {

  isLoading:boolean = false 
  isInitLoading:boolean = false
  isRefreshing:boolean = false
  
  lstPedidos:Pedido[]
  lstPedidosBackup:Pedido[]

  triggerRefreshSubs:Subscription
  pedidoListener:Subscription
  searchedListener:Subscription
  isFilteringListener:Subscription

  methods = new Methods

  refresherEvent = null

  constructor(private htcService: HtcService, private modalController: ModalController) { }
  

  ionViewWillEnter() {
    this.isInitLoading = true
    this.isLoading = true
    this.htcService.changeMode(1)
    this.htcService.serverGetPedidos()
  }

  ngOnInit() {  
    // this.htcService.socket.on('update', () => {
    //   this.isLoading = true
    //   this.htcService.serverGetPedidos()
    // })
    this.triggerRefreshSubs = this.htcService.triggerRefreshListener.subscribe(triggered => {
      if (triggered){
        this.isLoading = true
        this.htcService.serverGetPedidos()
      }else{
        console.log("Something Happen")
      }
    })

    this.pedidoListener = this.htcService.pedidoListener.subscribe(lPedidos => {
      this.lstPedidos = lPedidos
      this.lstPedidosBackup = lPedidos
  
      this.isLoading = false
      this.isInitLoading = false
      if (this.refresherEvent){
        this.refresherEvent.target.complete()
        this.refresherEvent = null
      }
      console.log(lPedidos)
    })

    this.searchedListener = this.htcService.searchedListener.subscribe(val => {
      if (val != ''){
        this.lstPedidos = this.methods.searchPedido(val, this.lstPedidosBackup)
      } else {
        this.lstPedidos = this.lstPedidosBackup
      }
    })

    this.isFilteringListener = this.htcService.isFilteringListerner.subscribe(isFiltering => {
      if (isFiltering){
        this.isInitLoading = true
        this.isLoading = true
      } 
      
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.pedidoListener.unsubscribe()
    this.isFilteringListener.unsubscribe()
    this.searchedListener.unsubscribe()
    this.triggerRefreshSubs.unsubscribe()
  }

  onRefresh(event) {
    this.isLoading = true
    this.refresherEvent = event
    this.htcService.serverGetPedidos()
  }

  setVisitTime(pedido){
    return  moment().format().split('T')[0] > moment(pedido['sv_visit_date']).format().split('T')[0]
  }

  onClick(pedido){
    if (pedido.length){
      pedido = pedido[0]
    }
    this.modalController.create({
      component:ModalPedidoComponent,
      componentProps:{'pedido':pedido}
    }).then(modal => {
      modal.present()
    })
  }

}
