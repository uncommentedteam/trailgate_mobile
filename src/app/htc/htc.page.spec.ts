import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HtcPage } from './htc.page';

describe('HtcPage', () => {
  let component: HtcPage;
  let fixture: ComponentFixture<HtcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HtcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
