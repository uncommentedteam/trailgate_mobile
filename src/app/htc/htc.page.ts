import { Pedido } from './../top/pedido';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ModalFilterComponent } from '../modal/modal-filter/modal-filter.component';
import { User } from '../top/user';
import { AuthService } from '../services/auth.service';
import { HtcService } from '../services/htc.service';

@Component({
  selector: 'app-htc',
  templateUrl: './htc.page.html',
  styleUrls: ['./htc.page.scss'],
})
export class HtcPage implements OnInit {

  curMode

  modeSub:Subscription
  pedidoSub:Subscription

  // CORE
  user:User

  listPedido:Pedido[]

  constructor(private htcService:HtcService,
              private authService:AuthService,
              private modelCtrl: ModalController) { }
  
            
  ngOnInit() {
    this.user = this.authService.getUser()
    this.modeSub = this.htcService.modeListener.subscribe(mNum => {
      this.curMode = mNum
      console.log("mode changed", mNum)
    })
  }
  

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.modeSub.unsubscribe()
  }

  async onFilterClick(){
    await this.modelCtrl.create({
      component:ModalFilterComponent,
      componentProps: {
        'user': this.user
      }
    }).then(modal => {
      modal.present()
      modal.onDidDismiss().then(data => {
        const filterObj = data.data['data']
        const isDone = data.data['done']
        if (isDone){
          this.htcService.serverFilterData(filterObj)
        }
        console.log(data.data)
      })
    })
  }

  onSearchChange(event){
    const val = event.target.value;
    this.htcService.searchaData(val)
  }

}

